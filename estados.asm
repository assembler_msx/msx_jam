

inicializa_estado:
	call inicializa_nodos ;inicializa la zona de memoria que alojará a los objetos

	
	ld a,[estado_actual]
	call JumpIndex
	dw logo_inicializa
	dw menu_inicializa
	dw juego_inicializa





	
logo_inicializa:
	call carga_graficos_logo
	
	ld hl,class_logo
	call inserta_objeto
	ret




menu_inicializa:
	call carga_graficos_menu
	
	ld hl,class_menu
	call inserta_objeto
	ret




	
juego_inicializa:
	call carga_graficos
	
	ld hl,mapeado
	ld [puntero_mapa_stage],hl ;este puntero guarda la dirección en ROM de la esquina superior izquierda del mapa
	call dibuja_mapa
	
	
	ld hl,class_prota		;insertamos el objeto prota
	call inserta_objeto
	ld hl,class_bola		;insertamos el objeto bola
	call inserta_objeto
	ld hl,class_ejemplo	;insertamos el objeto ejemplo
	ld c,180
	ld b,10
	call inserta_objeto

	call enascr ;habilitamos la pantalla
	
	ld hl,musica1
	call PT3_INIT ;inicializamos el player con la música apuntada por HL
	ld hl,sonidos
	call ayFX_SETUP ;inicializamos el reproductor de sonidos con el banco de sonidos apuntado por HL
	ret
	
	