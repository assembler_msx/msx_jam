


carga_graficos:

	ld a,12
	ld [0xf3e9],a
	xor a
	ld [0xf3ea],a
	ld [0xf3eb],a
        ld [0xf3db],a ;sonido_teclas
	call chgclr

	;limpiamos la pantalla
	ld hl,6144
	ld bc,32*24
	xor a
	call filvrm


	;cargamos los sprites
	ld hl,inicio_sprites
	ld de,0x3800
	ld bc,fin_sprites-inicio_sprites
	call ldirvm
	
	
	
	;cargamos los mismos tiles en los tres bloques
	ld a,4
	selecciona_segmento_8_megarom

	ld hl,mapeado_tiles_forma
	ld de,256*8*0
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm
	ld hl,mapeado_tiles_forma
	ld de,256*8*1
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm
	ld hl,mapeado_tiles_forma
	ld de,256*8*2
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm

	;cargamos los colores en los tres bloques
	ld hl,mapeado_tiles_color
	ld de,256*8*0+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	ld hl,mapeado_tiles_color
	ld de,256*8*1+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	ld hl,mapeado_tiles_color
	ld de,256*8*2+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	
	ret
	

dibuja_mapa:
	ld a,2
	selecciona_segmento_8_megarom
	ld a,3
	selecciona_segmento_a_megarom

	ld hl,[puntero_mapa_stage]
	
	ld b,alto_a_dibujar
	ld de,buffer_pantalla
.otra_fila:
	push bc
	
	
	
	ld bc,ancho_a_dibujar
	ldir
	push de
	ld de,ancho_total_pantallas-ancho_a_dibujar
	add hl,de
	pop de
	
	
	
	pop bc
	djnz .otra_fila

	ld hl,buffer_pantalla
	ld de,6144
	ld bc,768
	call ldirvm

	ret
	
	
lee_caracter_objeto:
	;esta función devuelve el caracter que está en las coordenadas del objeto
	;las coordenadas se desplazan en X+el registro C
	;y en Y+el registro B
	
	ld a,[ix+ACTOR.y]
	add b
	and 11111000b ;*8
	ld e,a
	ld d,0
	LEFT_DE ;*16
	LEFT_DE ;*32
	LEFT_DE ;*64
	LEFT_DE ;*128
	

	ld a,[ix+ACTOR.x]
	add c
	srl a ;/2
	srl a ;/4
	srl a ;/8

	ld hl,[puntero_mapa_stage]
	ADD_HL_A ;sumamos el desplazamiento de X

	add hl,de ;sumamos el desplazamiento de Y

	ld a,2
	selecciona_segmento_8_megarom
	ld a,3
	selecciona_segmento_a_megarom
	
	ld a,[hl]
	ret
	
	
	
comprueba_tile_duro:
	call lee_caracter_objeto
	cp TILES_DUROS ;la última fila de tiles del set se considera DURA
	jr c,.blando
.duro:
	xor a
	ret
.blando:
	xor a
	inc a
	ret
	