class_menu:
;Esta es la etiqueta que se debe cargar en HL antes de llamar a inserta_objeto para crear una instancia de esta CLASE


	macro_nombre_objeto 'menu    ' 
	;si depurando_nombres vale 1, esta macro genera un nombre que se podrá ver en la zona de memoria apuntada por IX durante la ejecución del MOV del objeto
	;si depurando_nombres vale 0, la misma macro rellena ese valor con 8 espacios en blanco
	;el nombre de objeto debe tener exactamente 8 caracteres. Si el nombre es más corto, rellenar con espacios
	dw class_menu_init 
		;etiqueta de la función que se ejecutará al insertar el objeto.
		;se utilizará para inicializar las variables propias de ese objeto
		;al llamar a inserta_objeto, se le podrán pasar valores a través de los registros:
		;A,B,C,D,E
		
	dw class_menu_mov
		;etiqueta de la función que se ejecutará en cada frame
		
	dw class_menu_hit
		;etiqueta de la función que se ejecutará cuando este objeto colisione con otro
		;las propiedades width,height,left y top definen la ventana de colisión de cada objeto
		;la ventana de colisión de un objeto puede ser más grande o pequeña que el gráfico de ese objeto
		;el motor revisa las colisiones entre todos los objetos que tengan el bit 


class_menu_init:

	ld hl,600
	ld [ix+MENU.tiempo],l
	ld [ix+MENU.tiempo+1],h
	
	
	ld [ix+MENU.empezando],0
	ld [ix+MENU.parpadeos],21
	
	;Para cada sprite del objeto, el motor calcula la suma de ACTOR.y, ACTOR.x, ACTOR.pat y ACTOR.col
	;con ACTOR.spriteN_y, ACTOR.spriteN_x, ACTOR.spriteN_pat y ACTOR.spriteN_col
	;dependiendo de las necesidades, bastará con modificar las propiedades del ACTOR, que afectarán a todos los sprites
	;o spriteN_propiedad, que solo afectarán a ese sprite

	
	ret
	
class_menu_mov:

	ld a,[ix+MENU.empezando]
	or a
	jr nz,.parpadeo_push_space_key
	
	ld a,[buffer_teclado8]
	bit 0,a
	jr z,.ha_pulsado_espacio

	ld l,[ix+MENU.tiempo]
	ld h,[ix+MENU.tiempo+1]
	dec hl
	ld [ix+MENU.tiempo],l
	ld [ix+MENU.tiempo+1],h
	
	ld a,h
	or l
	
	ret nz
	
	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
	call reset_estado
	
	ret
.ha_pulsado_espacio:
	ld [ix+MENU.empezando],a
	ld [ix+MENU.retardo_parpadeo],1
	ret
.parpadeo_push_space_key	

	dec [ix+MENU.retardo_parpadeo]
	ret nz
	ld [ix+MENU.retardo_parpadeo],4
	ld a,[ix+MENU.parpadeos]
	dec a
	jr z,.empezamos
	ld [ix+MENU.parpadeos],a
	bit 0,a
	jr z,.push_space_key_apagado
.push_space_key_encendido:
	ld hl,menu_tabla_nombres+19*32+17
	ld de,6144+19*32+17
	ld bc,14
	call ldirvm
	ret
.push_space_key_apagado:
	ld hl,menu_tabla_nombres+18*32+17
	ld de,6144+19*32+17
	ld bc,14
	call ldirvm
	ret
.empezamos:
	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
	call siguiente_estado
	ld a,4
	ld [pvida_prota],a
	ret

class_menu_hit:
	ret
	
	STRUCT MENU,ACTOR.variables
tiempo			word
empezando		byte
parpadeos		byte
retardo_parpadeo	byte
	ENDS
		