
VELOCIDAD_PROYECTIL EQU 4
RETARDO_ENTRE_BALAS equ 40


class_prota:
	macro_nombre_objeto 'prota   '
	dw class_prota_init
	dw class_prota_mov
	dw class_prota_hit


class_prota_init:
	ld [ix+ACTOR.num_sprites],2
	
	set SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],0*4
	ld [ix+ACTOR.sprite1_col],15
	ld [ix+ACTOR.sprite2_y],0
	ld [ix+ACTOR.sprite2_x],0
	ld [ix+ACTOR.sprite2_pat],1*4
	ld [ix+ACTOR.sprite2_col],9
	; ld [ix+ACTOR.sprite3_y],0
	; ld [ix+ACTOR.sprite3_x],0
	; ld [ix+ACTOR.sprite3_pat],2*4
	; ld [ix+ACTOR.sprite3_col],6
	
	
	

	ld [ix+ACTOR.x],100
	ld [ix+ACTOR.y],104
	ld [ix+ACTOR.pat],SPRITE_PROTA_PARADO


	ld [ix+ACTOR.left],4
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],8
	ld [ix+ACTOR.height],12
	
	
	ld [ix+PROTA.retardo_frame_paso],1
	ld [ix+PROTA.frame_paso],0
	ld [ix+PROTA.switches],0

	ld a,[pvida_prota]
	ld [ix+ACTOR.pvida],a



	;Almaceno la dirección donde están las variables del protagonista
	ld a,[ix+ACTOR.x]
	ld (x_prota),a
	
	ld a,[ix+ACTOR.y]
	ld (y_prota),a

	ret
	
class_prota_mov:

	ld [ix+ACTOR.num_sprites],2
	

	call prota_gestion_disparo


	call prota_gestion_andar



	call prota_gestion_pasos

	;Almaceno la dirección donde están las variables del protagonista
	ld a,[ix+ACTOR.x]
	ld (x_prota),a
	
	ld a,[ix+ACTOR.y]
	ld (y_prota),a

	ret

class_prota_hit:

	ld a,[iy+ACTOR.MATAPORCONTACTO] ;si el objeto con el que ha colisionado el prota tiene pvida, recibe un impacto
	cp 1
	ret nz;Si no mata por contacto me largo

	ld a,[iy+ACTOR.pvida] ;si el objeto con el que ha colisionado el prota tiene pvida, recibe un impacto
	or a
	ret z
	set SWITCHES_ACTOR.ELIMINAR,[iy+ACTOR.switches]
	dec [ix+ACTOR.pvida]
	jr nz,.vida_perdida
	call reset_estado
	ret
.vida_perdida:
	ld a,[ix+ACTOR.pvida]
	ld [pvida_prota],a
	call mismo_estado
	ret


prota_gestion_disparo:





	;todos los frames retardo el valor retardo_entre_proyectiles

	ld a,(retardo_entre_proyectiles)
	cp 0
	jr. z,.PuedoDisparar;Me salto si el retardo no es cero

	;Si no es cero lo disminuyo
	dec a
	ld (retardo_entre_proyectiles),a
	ret


.PuedoDisparar

	ld a,[buffer_teclado8]
	bit 0,a
	ret nz
	;Ok, he disparado


	;¿tengo balas?
	ld a,(num_proyectiles)
	cp 0
	jr nz ,.hayBalas;Me voy si no hay

	ld a,2
	call ayFX_INIT
	ret
.hayBalas


	ld bc,0

	;¿qué dirección?
	ld a,[buffer_teclado8]
	bit 7,a
	jr. nz,.noderecha
	ld b,VELOCIDAD_PROYECTIL ;Velocidad X	
.noderecha	
	ld a,[buffer_teclado8]
	bit 4,a
	jr. nz,.noizquierda
	ld b,-VELOCIDAD_PROYECTIL ;Velocidad X	
.noizquierda
	ld a,[buffer_teclado8]
	bit 5,a
	jr. nz,.noarriba
	ld c,-VELOCIDAD_PROYECTIL ;Velocidad Y
.noarriba
	ld a,[buffer_teclado8]
	bit 6,a
	jr. nz,.noabajo
	ld c,VELOCIDAD_PROYECTIL ;Velocidad Y
.noabajo




	ld a,b
	or c
	jr. nz,.noRepetirUltimaDireccion;No disparo si no he especificado una dirección
	;Última dirección, por si dispara sin moverse
	ld a,(ultima_direccion_disparo)
	ld b,a
	ld a,(ultima_direccion_disparo+1)
	ld c,a
.noRepetirUltimaDireccion


	;Guardo la última velocidad
	ld a,b;Velocidad X
	ld (ultima_direccion_disparo),a;
	ld a,c;Velocidad X
	ld (ultima_direccion_disparo+1),a;


	;En función del valor de b y c debo mostrar un patrón de sprite u otro
	ld a,b
	cp VELOCIDAD_PROYECTIL
	jr. z,.EsALaDerecha
	;Ok,NO es a la derecha


	cp -VELOCIDAD_PROYECTIL
	jr. z,.EsALaIzquierda

	;Si estoy aquí significa que es arriba o .abajo
	;¿Es arriba?
	ld a,c;Velocidad Y
	cp -VELOCIDAD_PROYECTIL
	jr. z,.EsArriba
	;Si estoy aquí significa que es Abajo 
	ld a,11
	jr. .patronDefinido
.EsArriba
	;Si estoy aquí significa que es Arriba 
	ld a,8
	jr. .patronDefinido

.EsALaIzquierda

	;¿Es arriba?
	ld a,c;Velocidad Y
	cp -VELOCIDAD_PROYECTIL
	jr. z,.EsIzquierdaArriba

	;¿Es abajo?
	cp VELOCIDAD_PROYECTIL
	jr. z,.EsIzquierdabajo

	;Si estoy aquí significa que es Izquierda 
	ld a,10
	jr. .patronDefinido

.EsIzquierdaArriba
	ld a,15
	jr. .patronDefinido

.EsIzquierdabajo
	ld a,14
	jr. .patronDefinido


	jr. .patronDefinido

.EsALaDerecha
	;¿Es arriba?
	ld a,c;Velocidad Y
	cp -VELOCIDAD_PROYECTIL
	jr. z,.EsDerechaArriba

	;¿Es abajo?
	cp VELOCIDAD_PROYECTIL
	jr. z,.EsDerechabajo

	;Si estoy aquí significa que es derecha 
	ld a,9
	jr. .patronDefinido

.EsDerechaArriba
	ld a,12
	jr. .patronDefinido

.EsDerechabajo
	ld a,13
	jr. .patronDefinido

.patronDefinido


[2]	sla a;Multiplico por 4 el patrón del 
	ld hl,class_proyectil		;insertamos el objeto bola
	call inserta_objeto

	ld a,RETARDO_ENTRE_BALAS;Inserto el retardo
	ld (retardo_entre_proyectiles),a

	ld a,(num_proyectiles)
	dec a
	 ld (num_proyectiles),a

	ld a,3
	call ayFX_INIT

	call actualizar_marcador_cargador

	ret
	


prota_gestion_andar:
;1000  Der Aba Arr Izq Del Ins Home Space
	res SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]


	ld a,[buffer_teclado8]
	bit 7,a
	jr. z,.resetearUltimaDireccion
	ld a,[buffer_teclado8]
	bit 4,a
	jr. z,.resetearUltimaDireccion
	ld a,[buffer_teclado8]
	bit 5,a
	jr. z,.resetearUltimaDireccion
	ld a,[buffer_teclado8]
	bit 6,a
	jr. z,.resetearUltimaDireccion

	jr. .NoResetearUltimaDireccion



.resetearUltimaDireccion
	xor a
	ld (ultima_direccion_disparo),a
	ld (ultima_direccion_disparo+1),a

.NoResetearUltimaDireccion


	ld a,[buffer_teclado8]
	bit 7,a
	call z,.derecha
	ld a,[buffer_teclado8]
	bit 4,a
	call z,.izquierda
	ld a,[buffer_teclado8]
	bit 5,a
	call z,.arriba
	ld a,[buffer_teclado8]
	bit 6,a
	call z,.abajo

	call comprueba_salida
	
	ret
.derecha:

	ld a,VELOCIDAD_PROYECTIL
	ld (ultima_direccion_disparo),a


	ld b,15
	ld c,11
	call comprueba_tile_duro
	ret z
	inc [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret nz
	ld [ix+PROTA.retardo_frame_paso],1
	set SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.izquierda:
	
	ld a,-VELOCIDAD_PROYECTIL
	ld (ultima_direccion_disparo),a



	ld b,15
	ld c,5
	call comprueba_tile_duro
	ret z

	dec [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret z
	ld [ix+PROTA.retardo_frame_paso],1
	res SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.arriba:
	ld a,-VELOCIDAD_PROYECTIL
	ld (ultima_direccion_disparo+1),a


	ld b,14
	ld c,6
	call comprueba_tile_duro
	ret z
	ld b,14
	ld c,10
	call comprueba_tile_duro
	ret z

	dec [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
.abajo:
	ld a,VELOCIDAD_PROYECTIL
	ld (ultima_direccion_disparo+1),a

	ld b,16
	ld c,6
	call comprueba_tile_duro
	ret z
	ld b,16
	ld c,10
	call comprueba_tile_duro
	ret z
	inc [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
	
		
prota_gestion_pasos:
	bit SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret z
	dec [ix+PROTA.retardo_frame_paso]
	ret nz
	ld [ix+PROTA.retardo_frame_paso],8
	
	ld a,[ix+PROTA.frame_paso]
	xor 00000001b 
	ld [ix+PROTA.frame_paso],a
	ld hl,frames_prota
	ADD_HL_A
	ld a,[hl]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	jr nz,.mirando_derecha
	add 4*4
.mirando_derecha:	
	ld [ix+ACTOR.pat],a
	
	ret


borrado_actores_pasivos:

	ld a,1
	ld (eliminar_actores_pasivos),a

	ret
	
	
crear_tesoro_pantalla16:
	ld hl,class_tesoro	;insertamos UN TESORO 
	ld c,120
	ld b,88
	call inserta_objeto
	
	ret


crear_actores_pasivos:

	; pantalla actual
	ld a,[pantalla_actual]		
	cp 16
	jr z,crear_tesoro_pantalla16
	
	
	
	and 00000011
	
	jp z,.pantalla0
	
	dec a
	jp z,.pantalla1
	
	dec a
	jp z,.pantalla2

	dec a
	jp z,.pantalla3
	

.pantalla0:	
	; bola
	ld hl,class_bola		;insertamos el objeto bola
	ld c,80
	ld b,124
	call inserta_objeto

	ld hl,class_cargador		;insertamos el objeto cargador
	ld c,60
	ld b,100
	call inserta_objeto

	jr .fin

.pantalla1:	

	; bola
	ld hl,class_bola		;insertamos el objeto bola
	ld c,80
	ld b,124
	call inserta_objeto

	; bola
	ld hl,class_bola		;insertamos el objeto bola
	ld c,30
	ld b,124
	call inserta_objeto


	ld hl,class_cargador		;insertamos el objeto cargador
	ld c,60
	ld b,100
	call inserta_objeto

	jr .fin

.pantalla2:	
	ld hl,class_bola		;insertamos el objeto bola
	ld c,180
	ld b,100
	call inserta_objeto

	ld hl,class_bola		;insertamos el objeto bola
	ld c,180
	ld b,150
	call inserta_objeto


	jr .fin

.pantalla3:	
	ld hl,class_bola		;insertamos el objeto bola
	ld c,180
	ld b,100
	call inserta_objeto

	ld hl,class_cargador		;insertamos el objeto cargador
	ld c,140
	ld b,40
	call inserta_objeto

.fin:
	;aprocecho para actualizar el marcador porque es el inicio de una nueva pantalla
	call actualizar_marcador_cargador	

	ret

	
comprueba_salida:
	ld a,[ix+ACTOR.x]
	cp 2
	jr c,.salida_por_izquierda
	cp 256-16
	jr nc,.salida_por_derecha
	ld a,[ix+ACTOR.y]
	cp 1
	jr c,.salida_por_arriba
	cp 192-16
	jr nc,.salida_por_abajo
	ret
.salida_por_izquierda:
	call borrado_actores_pasivos

	ld a,256-16
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	
	ld a,1
	call ayFX_INIT

	call eliminar_actores_pasivos_si_procede	
	
	call crear_actores_pasivos
	
	
	ret
.salida_por_derecha:
	call borrado_actores_pasivos

	ld a,3
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	
	call eliminar_actores_pasivos_si_procede	
	
	call crear_actores_pasivos
	
	ret

.salida_por_arriba:
	call borrado_actores_pasivos

	ld a,192-16
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	
	call eliminar_actores_pasivos_si_procede	
	
	call crear_actores_pasivos
	
	ret
.salida_por_abajo:
	call borrado_actores_pasivos

	ld a,3
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT


	call eliminar_actores_pasivos_si_procede	
	
	call crear_actores_pasivos
	

	ret
		

		
		
		
		
SPRITE_PROTA_PARADO equ 0*4

SPRITE_PROTA_ANDANDO_0 equ 0*4
SPRITE_PROTA_ANDANDO_1 equ 2*4

	STRUCT SWITCHES_PROTA
	;se utilizan tipos de dato byte, pero se utilizan como posiciones de bit dentro de PROTA.switches
ANDANDO			byte
LIBRE1			byte
LIBRE2			byte
LIBRE3			byte
DERECHA			byte
LIBRE5			byte
LIBRE6			byte
LIBRE7			byte
	ENDS


frames_prota:
	db SPRITE_PROTA_ANDANDO_0
	db SPRITE_PROTA_ANDANDO_1

	STRUCT PROTA,ACTOR.variables
retardo_frame_paso		byte
frame_paso			byte
switches			byte
	ENDS




