class_proyectil:
	macro_nombre_objeto 'proyecti'
	dw class_proyectil_init
	dw class_proyectil_mov
	dw class_proyectil_hit


class_proyectil_init:

;	ld a,9
;[2]	sla a

	set SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	

	ld [ix+ACTOR.num_sprites],1
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0

	ld [ix+ACTOR.sprite1_pat],a
	ld [ix+ACTOR.sprite1_col],3	

	ld [ix+PROYECTIL.frame],0
	ld [ix+PROYECTIL.retardo_frame_paso],1
	ld [ix+PROYECTIL.retardo_paso],1

	ld [ix+ACTOR.velocidadX],b ;parámetro pasado a través del registro B antes de llamar a inserta_objeto
	ld [ix+ACTOR.velocidadY],c ;parámetro pasado a través del registro C antes de llamar a inserta_objeto
	ld [ix+ACTOR.pat],0*4

	ld [ix+ACTOR.left],2
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],12
	ld [ix+ACTOR.height],12

	ld [ix+ACTOR.pvida],1

	ld [ix+ACTOR.MATAPORCONTACTO],0;NO MATA POR CONTACTO


	; push ix
	; call setIXProtagonista	
	; ;Ahora IX apunta al protagonista
	; ld c,[ix+ACTOR.x] ;Obtengo las coordenadas del protagonista
	; ld b,[ix+ACTOR.y]  ;Obtengo las coordenadas del protagonista
	; pop ix
	
	; ld [ix+ACTOR.x],c ;Guardo las coordenadas del protagonista
	; ld [ix+ACTOR.y],b ;Guardo las coordenadas del protagonista


	ld a,(x_prota)
	 ld [ix+ACTOR.x],a ;Obtengo las coordenadas del protagonista
	ld a,(y_prota)
	 ld [ix+ACTOR.y],a  ;Obtengo las coordenadas del protagonista


	ret
	
class_proyectil_mov:


	ld b,[ix+ACTOR.velocidadX]
	ld a,[ix+ACTOR.x]
	add a,b
	ld [ix+ACTOR.x],a

	ld b,[ix+ACTOR.velocidadY]
	ld a,[ix+ACTOR.y]
	add a,b
	ld [ix+ACTOR.y],a


	;Chequeo si me paso
	ld a,[ix+ACTOR.x]
	cp 10
	jr. c,.eliminarProyectil

	ld a,[ix+ACTOR.x]
	cp 236
	jr. nc,.eliminarProyectil

	ld a,[ix+ACTOR.y]
	cp 10
	jr. c,.eliminarProyectil

	ld a,[ix+ACTOR.y]
	cp 236
	jr. nc,.eliminarProyectil

	ret
.eliminarProyectil

	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]

	;dec [ix+PROYECTIL.retardo_frame_paso]
	; ret nz
	
	; ld a,[ix+PROYECTIL.frame]
	; inc a
	; and 00000011b
	; ld [ix+PROYECTIL.frame],a


	; ld hl,proyectil_frames
	; ADD_HL_A
	; ld a,[hl]
	; ld [ix+ACTOR.pat],a

	; ld hl,proyectil_retardo_frames
	; ld a,[ix+PROYECTIL.frame]
	; ADD_HL_A
	; ld a,[hl]
	; ld [ix+PROYECTIL.retardo_frame_paso],a


	ret


class_proyectil_hit:



	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]

;Voy a bajar la vida del actor pasivo
	ld a,[iy+ACTOR.pvida]
	dec a
	ld [iy+ACTOR.pvida],a
	cp 0
	ret nz
	;Elimiono al actor porque no le queda vida
	set SWITCHES_ACTOR.ELIMINAR,[iy+ACTOR.switches]

	ret
	
	
	
	

proyectil_frames: db 0*4
proyectil_retardo_frames: db 9


	STRUCT PROYECTIL,ACTOR.variables
retardo_frame_paso	byte
retardo_paso		byte
frame			byte
	ENDS
	