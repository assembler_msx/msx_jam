VIDA_BOLA EQU 5

class_bola:
	macro_nombre_objeto 'bola    '
	dw class_bola_init
	dw class_bola_mov
	dw class_bola_hit

;Entrada: 
;	 Reg c (Position X)
;	 Reg b (Position Y)
;Salida: 
;	 
class_bola_init:


	set SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	

	ld [ix+ACTOR.num_sprites],1
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],16*4
	ld [ix+ACTOR.sprite1_col],12

	ld [ix+BOLA.frame],0
	ld [ix+BOLA.retardo_frame_paso],1
	ld [ix+BOLA.retardo_paso],1

	ld [ix+ACTOR.x],c			; x <- c
	ld [ix+ACTOR.y],b			; y <- b
	ld [ix+ACTOR.pat],0*4

	ld [ix+ACTOR.left],2
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],12
	ld [ix+ACTOR.height],12

	ld [ix+ACTOR.pvida],VIDA_BOLA

	ld [ix+ACTOR.MATAPORCONTACTO],1

	ret
	
class_bola_mov:
	dec [ix+BOLA.retardo_frame_paso]
	ret nz
	
	ld a,[ix+BOLA.frame]
	inc a
	and 00000011b
	ld [ix+BOLA.frame],a


	ld hl,bola_frames
	ADD_HL_A
	ld a,[hl]
	ld [ix+ACTOR.pat],a

	ld hl,bola_retardo_frames
	ld a,[ix+BOLA.frame]
	ADD_HL_A
	ld a,[hl]
	ld [ix+BOLA.retardo_frame_paso],a


	; movement
	ld a,[x_prota]
	
	ld b,[ix+ACTOR.x]
	sub b
	jr c,.subtractX
.addX:
	ld a,[ix+ACTOR.x]
	inc a
	ld [ix+ACTOR.x],a	
	jr .finCalc
.subtractX:
	ld a,[ix+ACTOR.x]
	dec a
	ld [ix+ACTOR.x],a
.finCalc:	


	ld a,[y_prota]
	
	ld b,[ix+ACTOR.y]
	sub b
	jr c,.subtractY
.addY:
	ld a,[ix+ACTOR.y]
	inc a
	ld [ix+ACTOR.y],a	
	jr .finCalc2
.subtractY:
	ld a,[ix+ACTOR.y]
	dec a
	ld [ix+ACTOR.y],a
.finCalc2:	

/*	
	call generate_rnd_number
	ld a,[rnd_number]
	
	and 00000011b
	and a
	jp nz,.secondMovement
.firstMovement:	
	ld a,[ix+ACTOR.x]
	inc a
	ld [ix+ACTOR.x],a			
	ld a,[ix+ACTOR.y]
	inc a
	ld [ix+ACTOR.y],a			
	jr .fin
.secondMovement:	
	ld a,[ix+ACTOR.x]
	dec a
	ld [ix+ACTOR.x],a			
	ld a,[ix+ACTOR.y]
	inc a
	ld [ix+ACTOR.y],a			
.fin	
*/

	ret


class_bola_hit:
	ret
	
	
	
	

bola_frames: db 0*4,1*4,2*4,1*4
bola_retardo_frames: db 9,3,5,3


	STRUCT BOLA,ACTOR.variables
retardo_frame_paso	byte
retardo_paso		byte
frame			byte
	ENDS
	