class_tesoro:
	macro_nombre_objeto 'tesoro  '
	dw class_tesoro_init
	dw class_tesoro_mov
	dw class_tesoro_hit


class_tesoro_init:


	set SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	

	ld [ix+ACTOR.num_sprites],2
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],20*4
	ld [ix+ACTOR.sprite1_col],5

	ld [ix+TESORO.frame],0
	ld [ix+TESORO.retardo_frame_paso],1
	ld [ix+TESORO.retardo_paso],11

	ld [ix+ACTOR.x],c ;parámetro pasado a través del registro C antes de llamar a inserta_objeto
	ld [ix+ACTOR.y],b ;parámetro pasado a través del registro B antes de llamar a inserta_objeto
	ld [ix+ACTOR.pat],0*4

	ld [ix+ACTOR.left],2
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],12
	ld [ix+ACTOR.height],12

	ld [ix+ACTOR.pvida],1

	ld [ix+ACTOR.MATAPORCONTACTO],0;NO MATA POR CONTACTO

	ret
	
class_tesoro_mov:




	ret


class_tesoro_hit:

	;Cuando choco contra el tesoro aumento el contador de balas
	ld a,1
	call ayFX_INIT


	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]

	ret
	
	
	
	

tesoro_frames: db 0*4
tesoro_retardo_frames: db 9


	STRUCT TESORO,ACTOR.variables
retardo_frame_paso	byte
retardo_paso		byte
frame			byte
	ENDS
	