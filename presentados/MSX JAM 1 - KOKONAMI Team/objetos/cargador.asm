INCREMENTO_PROYECTILES_CARGADOR EQU 5 ;El número de proyectiles que aumentan al cargar un cargardor

class_cargador:
	macro_nombre_objeto 'cargador'
	dw class_cargador_init
	dw class_cargador_mov
	dw class_cargador_hit


class_cargador_init:


	set SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	

	ld [ix+ACTOR.num_sprites],1
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],19*4
	ld [ix+ACTOR.sprite1_col],3	

	ld [ix+CARGADOR.frame],0
	ld [ix+CARGADOR.retardo_frame_paso],1
	ld [ix+CARGADOR.retardo_paso],1

	ld [ix+ACTOR.x],c ;parámetro pasado a través del registro C antes de llamar a inserta_objeto
	ld [ix+ACTOR.y],b ;parámetro pasado a través del registro B antes de llamar a inserta_objeto
	ld [ix+ACTOR.pat],0*4

	ld [ix+ACTOR.left],2
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],12
	ld [ix+ACTOR.height],12

	ld [ix+ACTOR.pvida],1

	ld [ix+ACTOR.MATAPORCONTACTO],0;NO MATA POR CONTACTO

	ret
	
class_cargador_mov:
	;dec [ix+CARGADOR.retardo_frame_paso]
	; ret nz
	
	; ld a,[ix+CARGADOR.frame]
	; inc a
	; and 00000011b
	; ld [ix+CARGADOR.frame],a


	; ld hl,cargador_frames
	; ADD_HL_A
	; ld a,[hl]
	; ld [ix+ACTOR.pat],a

	; ld hl,cargador_retardo_frames
	; ld a,[ix+CARGADOR.frame]
	; ADD_HL_A
	; ld a,[hl]
	; ld [ix+CARGADOR.retardo_frame_paso],a


	ret


class_cargador_hit:

	;Cuando choco contra el cargador aumento el contador de balas
	ld a,(num_proyectiles)
	add INCREMENTO_PROYECTILES_CARGADOR
	ret c;Me voy si me paso de 255
	ld (num_proyectiles),a


	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]


	ld a,1
	ld (eliminar_actores_pasivos),a

	; REgistro hl;Numero de tile a modificar
	; Registro a; Nuevo valor

	CALL actualizar_marcador_cargador
	
	ret
	
	
	
	

cargador_frames: db 0*4
cargador_retardo_frames: db 9


	STRUCT CARGADOR,ACTOR.variables
retardo_frame_paso	byte
retardo_paso		byte
frame			byte
	ENDS
	