carga_graficos_menu:

	;limpiamos la pantalla
	ld hl,6144
	ld bc,32*24
	xor a
	call filvrm

	ld a,12
	ld [0xf3e9],a
	xor a
	ld [0xf3ea],a
	ld [0xf3eb],a
	
	call 0x0062 ;CHGCLR cambia el color de letras en función a los valores de FORCLR, BAKCLR y BDRCLR


	ld a,4
	selecciona_segmento_8_megarom

	;cargamos los mismos tiles en los tres bloques
	ld hl,menu_tiles_forma
	ld de,256*8*0
	ld bc,fin_menu_tiles_forma-menu_tiles_forma
	call ldirvm

	ld hl,menu_tiles_forma
	ld de,256*8*1
	ld bc,fin_menu_tiles_forma-menu_tiles_forma
	call ldirvm

	ld hl,menu_tiles_forma
	ld de,256*8*2
	ld bc,fin_menu_tiles_forma-menu_tiles_forma
	call ldirvm
	
	ld hl,menu_tiles_color
	ld de,256*8*0+8192
	ld bc,fin_menu_tiles_color-menu_tiles_color
	call ldirvm
	
	ld hl,menu_tiles_color
	ld de,256*8*1+8192
	ld bc,fin_menu_tiles_color-menu_tiles_color
	call ldirvm
	
	ld hl,menu_tiles_color
	ld de,256*8*2+8192
	ld bc,fin_menu_tiles_color-menu_tiles_color
	call ldirvm

	ld hl,menu_tabla_nombres
	ld de,6144
	ld bc,fin_menu_tabla_nombres-menu_tabla_nombres
	call ldirvm
	
	
	ret
	