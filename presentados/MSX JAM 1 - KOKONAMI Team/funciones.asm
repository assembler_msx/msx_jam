inicializa_modo_grafico:
	
	ld a,2
	call chgmod
	


	ld c,1
	ld a,[rg1sav]
	or 0x62 ;<-2 para activar el bit de sprites 16x16
	ld b,a 
	call wrtvdp 
	
	ret
lee_teclado:


;tabla de teclas:
;Bits  7 6 5 4 3 2 1 0

;0000  7 6 5 4 3 2 1 0
;0001  ; [ @ Y ^ - 9 8     <- Y es el simbolo del Yen
;0010  B A - / . , ] :
;0011  J I H G F E D C
;0100  R Q P O N M L K
;0101  Z Y X W V U T S
;0110  F3 F2 F1 Kana Caps Graph Ctrl Shift
;0111  Return Select BS Stop TAB ESC F5 F4
;1000  Der Aba Arr Izq Del Ins Home Space

        ld hl,buffer_teclado
        ld b,9
        ld c,0
.otralinea:
        in a,[0xaa]
        and 0xf0
        or c
        out [0xaa],a
        in a,[0xa9]
        ld [hl],a
        inc hl
        inc c
        djnz .otralinea
        ret
	
busca_slotset:
	;busca_slot
        call rslreg
        rrca
        rrca
        and 3
        ld c,a
        ld b,0
        ld hl,0FCC1h
        add hl,bc
        ld a,[hl]
        and 0x80
        or c
        ld c,a
        inc hl
        inc hl
        inc hl
        inc hl
        ld a,[hl]
        and 0Ch
        or c;
        ld h,080h
        ld [slotvar_megarom],a
        jp enaslt

divide_d_e:
	xor	a
   	ld	b, 8

.loop:
   	sla	d
 	rla
   	cp	e
   	jr	c, $+4
   	sub	e
   	inc	d
   
   	djnz	.loop
   
   	ret


	

resetea_memoria:
	ld hl,inicio_variables_inicializar_a_cero
	ld de,inicio_variables_inicializar_a_cero+1
	ld bc,fin_variables_inicializar_a_cero-inicio_variables_inicializar_a_cero
	xor a
	ld [hl],a
	ldir
	ret


	
inicializa_interrupcion:
;DESACTIVA LA INTERRUPCION DEL VDP

	push hl

        ld hl,bloque_im2
        ld de,bloque_im2+1
        ld [hl],0xee
        ld bc,256
        ldir
        ld a,0xc3 ;jp
        ld [salto_a_isr],a
        pop hl
        ;ld hl,interrupcion
        ld [salto_a_isr+1],hl



        ld a,0xed
        ld i,a
        im 2
	ret
	


interrupcion:
        push af

	xor a
	out [0x99],a
	ld a,15+128
	out [0x99],a
	
	in a,(0x99)     
	bit 7,a         
	jp z,.notFromVDP

        push bc
        push de
        push hl
        push ix
        push iy
	ex af,af'	;'
	exx
	push af
	push bc
	push de
	push hl

	ld a,1
	ld [interrupcion_ejecutada],a 
	call PT3_ROUT
	call PT3_PLAY
	call ayFX_PLAY

        pop hl
        pop de
        pop bc
	pop af
	exx
	ex af,af'	;'
	
        pop iy
        pop ix
        pop hl
        pop de
        pop bc
.notFromVDP:
        pop af

        reti	


;llama a una lista de funciones con indice A, y que deberá estar colocada inmediatamente después del "call JumpIndex"
;Manuel Pazos (Codigo fuente de Metal Gear)
JumpIndex:
	pop hl				    ; Pointer to list
	add a,a
	ADD_HL_A
	ld e, (hl)
	inc hl
	ld d, (hl)			    ; DE = Address to jump
	ex de,	hl
	jp (hl)
	;el RET de la función que se llama devolverá el control a la función que llamó a la función donde está el call JumpIndex
	
;Fernando García
siguiente_estado:
	xor     a
	ld	(estado_juego),a
	ld      hl, estado_actual
	inc     (hl)                    ; Pasa al siguiente estado
	inc     hl
	ld      (hl),a                  ; Subestado inicial
	ret             
reset_estado:
	xor     a
	ld	(estado_juego),a
	ld      hl, estado_actual
	ld	(hl),a
	inc     hl
	ld      (hl),a                  ; Subestado inicial
	ret             	
mismo_estado:
	xor     a
	ld	(estado_juego),a
	ret             		