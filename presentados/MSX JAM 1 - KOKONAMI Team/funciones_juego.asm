


carga_graficos:

	ld a,12
	ld [0xf3e9],a
	xor a
	ld [0xf3ea],a
	ld [0xf3eb],a
        ld [0xf3db],a ;sonido_teclas
	call chgclr

	;limpiamos la pantalla
	ld hl,6144
	ld bc,32*24
	xor a
	call filvrm


	;cargamos los sprites
	ld hl,inicio_sprites
	ld de,0x3800
	ld bc,fin_sprites-inicio_sprites
	call ldirvm
	
	
	
	;cargamos los mismos tiles en los tres bloques
	ld a,4
	selecciona_segmento_8_megarom

	ld hl,mapeado_tiles_forma
	ld de,256*8*0
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm
	ld hl,mapeado_tiles_forma
	ld de,256*8*1
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm
	ld hl,mapeado_tiles_forma
	ld de,256*8*2
	ld bc,fin_mapeado_tiles_forma-mapeado_tiles_forma
	call ldirvm

	;cargamos los colores en los tres bloques
	ld hl,mapeado_tiles_color
	ld de,256*8*0+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	ld hl,mapeado_tiles_color
	ld de,256*8*1+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	ld hl,mapeado_tiles_color
	ld de,256*8*2+8192
	ld bc,fin_mapeado_tiles_color-mapeado_tiles_color
	call ldirvm
	
	ret
	

dibuja_mapa:
	ld a,2
	selecciona_segmento_8_megarom
	ld a,3
	selecciona_segmento_a_megarom

	ld hl,[puntero_mapa_stage]
	
	ld b,alto_a_dibujar
	ld de,buffer_pantalla
.otra_fila:
	push bc
	
	
	
	ld bc,ancho_a_dibujar
	ldir
	push de
	ld de,ancho_total_pantallas-ancho_a_dibujar
	add hl,de
	pop de
	
	
	
	pop bc
	djnz .otra_fila

	ld hl,buffer_pantalla
	ld de,6144
	ld bc,768
	call ldirvm


	call establece_pantalla

	ret
	
	
lee_caracter_objeto:
	;esta función devuelve el caracter que está en las coordenadas del objeto
	;las coordenadas se desplazan en X+el registro C
	;y en Y+el registro B
	
	ld a,[ix+ACTOR.y]
	add b
	and 11111000b ;*8
	ld e,a
	ld d,0
	LEFT_DE ;*16
	LEFT_DE ;*32
	LEFT_DE ;*64
	LEFT_DE ;*128
	

	ld a,[ix+ACTOR.x]
	add c
	srl a ;/2
	srl a ;/4
	srl a ;/8

	ld hl,[puntero_mapa_stage]
	ADD_HL_A ;sumamos el desplazamiento de X

	add hl,de ;sumamos el desplazamiento de Y

	ld a,2
	selecciona_segmento_8_megarom
	ld a,3
	selecciona_segmento_a_megarom
	
	ld a,[hl]
	ret
	
	
	
comprueba_tile_duro:
	call lee_caracter_objeto
	cp TILES_DUROS ;la última fila de tiles del set se considera DURA
	jr c,.blando
.duro:
	xor a
	ret
.blando:
	xor a
	inc a
	ret

establece_pantalla
	push af
	push hl
	push de

	call obtener_pantalla_actual
	ld (pantalla_actual),a

	pop de
	pop hl
	pop af
	ret 



;*****************************************************************************************************************
;*   ESTA RUTINA LO QUE HACE ES QUE TE DECUELVE EN EL REGISTTRO A EL NÚMERO DE LA PANTALLA ACTUAL
;*****************************************************************************************************************



obtener_pantalla_actual
	ld hl,[puntero_mapa_stage]

	ld de,#8000
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla1
	ld a,1
	ret
.noEsLaPantalla1

	ld de,#8020
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla2
	ld a,2
	ret
.noEsLaPantalla2

	ld de,#8040
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla3
	ld a,3
	ret
.noEsLaPantalla3

	ld de,#8060
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla4
	ld a,4
	ret
.noEsLaPantalla4

	ld de,#8C00
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla5
	ld a,5
	ret
.noEsLaPantalla5

	ld de,#8C20
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla6
	ld a,6
	ret
.noEsLaPantalla6


	ld de,#8C40
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla7
	ld a,7
	ret
.noEsLaPantalla7


	ld de,#8C60
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla8
	ld a,8
	ret
.noEsLaPantalla8

	ld de,#9800
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla9
	ld a,9
	ret
.noEsLaPantalla9

	ld de,#9820
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla10
	ld a,10
	ret
.noEsLaPantalla10

	ld de,#9840
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla11
	ld a,11
	ret
.noEsLaPantalla11

	ld de,#9860
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla12
	ld a,12
	ret
.noEsLaPantalla12

	ld de,#A400
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla13
	ld a,13
	ret
.noEsLaPantalla13


	ld de,#A420
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla14
	ld a,14
	ret
.noEsLaPantalla14

	ld de,#A440
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla15
	ld a,15
	ret
.noEsLaPantalla15

	ld de,#A460
	call compararHL_DE
	cp 1
	jr. nz,.noEsLaPantalla16
	ld a,16
	ret
.noEsLaPantalla16


	ret


compararHL_DE
	xor a
	push hl
	push de
	sbc hl,de
	ld a,h
	or l
	cp 0
	jr nz,.noSonIguales
	ld a,1
.noSonIguales
	pop de
	pop hl

;#0080

	ret	


; Fast RND
;
; An 8-bit pseudo-random number generator,
; using a similar method to the Spectrum ROM,
; - without the overhead of the Spectrum ROM.
;
; R = random number seed
; an integer in the range [1, 256]
;
; R -> (33*R) mod 257
;
; S = R - 1
; an 8-bit unsigned integer

; entrada
;			Ningun parametro
; salida
;			variable JIFFY
; modifica
;			af, bc
generate_rnd_number:
	ld 		a, (rnd_number)
	ld 		b, a 

	rrca ; multiply by 32
	rrca
	rrca
	xor 	0x1f

	add 	a, b
	sbc 	a, 255 ; carry
	
	ld 		(rnd_number), a
	ret

