procesa_nodos:



.nuevo_ciclo:
	ld ix,[nodos_ocupados]
        ld a,ixh
        inc a;cp 0xff
        ret z

	ld hl,objetos_pasivos
	ld (indice_objetos_pasivos),hl
	ld hl,objetos_activos
	ld (indice_objetos_activos),hl

	ld hl,0
	ld [num_objetos_activos],hl ;num_objetos_activos,num_objetos_pasivos


	ld hl,0xffff
	ld [nodo_anterior],hl
.nuevo_nodo:
	ld [nodo_anterior_aux],ix
	
	


	;ejecutamos metodo_exec
	ld [nodo_padre],ix


	;se comprueba el bit de eliminacion antes de ejecutar el objeto, para el caso en el que se haya activado dentro de COMPRUEBA_COLISIONES
	;que esta fuera de este ciclo y se ejecuta despu�s, pero los efectos quedan para el siguiente ciclo. Ej:
	;Ciclo 1 objeto en cuesti�n con el bit de eliminacion apagado
	;comprobar colisiones: se activa el bit, PERO NO SE ELIMINA
	;Ciclo 2 El bit de eliminaci�n est� activado. Sin este control, habr�a que controlar el caso en cada objeto
	;para evitar probleas caso hijo->padre.hijos--, se controla desde aqu�
	;Se podr�a eliminar el nodo dentro de la misma rutina COMPRUEBA_COLISIONES, pero mejor hacerlo solo en un sitio
	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
	jp nz,.objeto_eliminado_durante_comprueba_colisiones

	
	ld l,[ix+ACTOR.EVENTO_MOV]
	ld h,[ix+ACTOR.EVENTO_MOV+1]
	call llamada_funcion_hl


	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
.objeto_eliminado_durante_comprueba_colisiones:
	ld l,[ix+ACTOR.NODO_SIGUIENTE]
	ld h,[ix+ACTOR.NODO_SIGUIENTE+1]
	push hl
	jr nz,.liberamos_nodo
.objeto_dentro_de_pantalla:

	call dibuja_sprites_nodo
	
.objeto_fuera_pantalla:	
.sprites_fuera_pantalla_eliminados:	
.no_hay_sprites_que_eliminar_fuera_pantalla:	
	;solo cambiamos el nodo anterior cuando no eliminamos. Si eliminamos, el anterior sigue siendo el mismo (no el que se ha eliminado)
	ld hl,[nodo_anterior_aux]
	ld [nodo_anterior],hl
	jr .no_liberamos_nodo
.liberamos_nodo:
	call libera_slot_sprites
        call libera_nodo
        jr .nodo_liberado
.no_liberamos_nodo:
	bit SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	jr z,.no_es_activo
.es_activo:
	ld hl,num_objetos_activos
	inc [hl]
	ld hl,[indice_objetos_activos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_activos],hl
	jr .no_es_pasivo
.no_es_activo:
	bit SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	jr z,.no_es_pasivo
.es_pasivo:
	ld hl,num_objetos_pasivos
	inc [hl]
	ld hl,[indice_objetos_pasivos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_pasivos],hl

.no_es_pasivo:
.objeto_fuera_de_pantalla_es_ignorado:

.nodo_liberado:

	pop hl
	ld b,l
	ld a,h
	cp 0xff
	jr z,.acabamos

	
	
	ld ixh,a
	ld ixl,b

	jp .nuevo_nodo

.acabamos:

	ret


inserta_nodo_al_final:
;devuelve IX
; si no hay nodos libres, IX vale -1

	ld ix,[nodos_libres]
	ld a,ixh
	cp 0xff
	ret z

;        push hl	;se almacena porque es la direcci�n del objeto, y la necesitamos luego

;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION
;NO SE DEBE USAR HL EN TODA LA FUNCION


	;nodos libres apunta al nodo siguiente del nodo nuevo
	ld e,[ix+ACTOR.NODO_SIGUIENTE]
	ld d,[ix+ACTOR.NODO_SIGUIENTE+1]
	ld [nodos_libres],de

	;el siguiente del nodo nuevo siempre es XX ya que insertamos al final
	ld [ix+ACTOR.NODO_SIGUIENTE],0xff
	ld [ix+ACTOR.NODO_SIGUIENTE+1],0xff

	;el que era ultimo nodo, apunta al nuevo
	ld iy,[ultimo_nodo_ocupado]
	ld a,iyh
	inc a ;cp 0xff
	jr z,.lista_vacia
	ld a,ixl
        ld [iy+ACTOR.NODO_SIGUIENTE],a
	ld a,ixh
	ld [iy+ACTOR.NODO_SIGUIENTE+1],a
	jr .enlace_realizado
.lista_vacia:
	ld [nodos_ocupados],ix


.enlace_realizado:
	;ultimo_nodo_ocupado ahora apunta al nodo que nos llevamos

	ld [ultimo_nodo_ocupado],ix


	ret


libera_nodo:
;entrada IX: nodo a liberar



	ld iy,[nodo_anterior]
	ld a,iyh
	inc a ;cp 0xff

	
	if depurando_nombres=1
		ld [ix+ACTOR.NOMBRE_OBJETO+0],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+1],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+2],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+3],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+4],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+5],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+6],'X'
		ld [ix+ACTOR.NOMBRE_OBJETO+7],'X'
	endif
	
	
	
	
	ld e,[ix+ACTOR.NODO_SIGUIENTE]
	ld d,[ix+ACTOR.NODO_SIGUIENTE+1]

	jr z,.hay_que_eliminar_el_primer_nodo

.no_hay_que_eliminar_el_primer_nodo:
	;el siguiente nodo del nodo anterior apunta al siguiente nodo del actual
	ld [iy+ACTOR.NODO_SIGUIENTE],e
	ld [iy+ACTOR.NODO_SIGUIENTE+1],d
	jr .nodo_intermedio_eliminado
.hay_que_eliminar_el_primer_nodo:
	ld [nodos_ocupados],de


.nodo_intermedio_eliminado:

	ld a,[ix+ACTOR.NODO_SIGUIENTE+1]
	inc a ;cp 0xff
	jr nz,.no_es_el_ultimo

	ld [ultimo_nodo_ocupado],iy


.no_es_el_ultimo:

	;el siguiente nodo del nodo actual apunta al primer nodo libre
	ld de,[nodos_libres]
	ld [ix+ACTOR.NODO_SIGUIENTE],e
	ld [ix+ACTOR.NODO_SIGUIENTE+1],d

	ld [nodos_libres],ix
	ld hl,num_objetos
	dec [hl]

	ret



inicializa_nodos:

	xor a
	ld hl,nodos
	ld [hl],a
	ld de,nodos+1
	ld bc,NODOS_MAX*NODOS_SIZE-1
	ldir


	ld hl,0xffff
	ld [aux],hl
	ld [nodos_ocupados],hl
	ld [ultimo_nodo_ocupado],hl
	xor a
	ld [num_objetos],a
        ld a,125-32
        ld [sprites_frame_anterior],a


	ld bc,NODOS_MAX

	ld ix,nodos
	ld [nodos_libres],ix
.bucle_nodos:
	ld hl,[aux]
	ld [aux],ix ;el nodo actual, ser� el pr�ximo NODO_ANTERIOR
	ld hl,NODOS_SIZE ;El siguiente nodo esta, en la inicializaci�n, a NODO_SIZE bytes del actual
	ld d,ixh
	ld e,ixl
	add hl,de
	ld [ix+ACTOR.NODO_SIGUIENTE],l
	ld [ix+ACTOR.NODO_SIGUIENTE+1],h
	ld d,h
	ld e,l
	ld ixl,e
	ld ixh,d
	dec bc
	ld a,b
	or c
	jr nz,.bucle_nodos
	ld [ix+ACTOR.NODO_SIGUIENTE-NODOS_SIZE],-1
	ld [ix+ACTOR.NODO_SIGUIENTE+1-NODOS_SIZE],-1

;el �ltimo nodo.siguiente debe apuntar a -1
        ld hl,objetos_activos
        ld de,objetos_activos+1
        ld [hl],0
        ld bc,2*NODOS_MAX-1
        ldir
        ld hl,objetos_pasivos
        ld de,objetos_pasivos+1
        ld [hl],0
        ld bc,2*NODOS_MAX-1
        ldir
	
	xor a
	ld [slots_sprites_libres_x4],a
	ld [slots_sprites_libres_x2],a
	ld [slots_sprites_libres_x1],a
	
	ret


inserta_objeto:
;la insercci�n se hace al final de la lista
;la ejecuci�n siempre se hace en orden
;los sprites se dibujan en diferentes bloques
;la prioridad del sprite se define en el objeto
;los planos 0 2 y 4 se usar�n normalmente para los enemigos, balas, etc
;los planos 1 y 3 se utilizar�n para asegurar una posici�n en concreto al protagonista
;por ejemplo, paulo est� normalmente en el 0, por encima de todo
;pero al morir, tiene que aparecer por encima de los sprites de suelo (losas), pero por debajo de los sprites de enemigos (bolas, murci�lagos...)
;HL: objeto a insertar
;BC y DE: parametros
;IX e IY se mantienen. Para referenciar al objeto reci�n creado, se usa la variable "ultimo_objeto_creado"

        ld [parametros_desde_mapa_a],a
        ld a,e
        ld [parametros_desde_mapa_e],a
        ld a,d
        ld [parametros_desde_mapa_d],a
        ld a,c
        ld [parametros_desde_mapa_c],a
        ld a,b
        ld [parametros_desde_mapa_b],a
        push ix
        push iy
	call inserta_nodo_al_final
	ld [ultimo_objeto_creado],ix

	ld a,ixh
	cp 0xff
	pop iy
	pop ix
	ret z ;,.sinmemoria   IX ya vale 0xffff
        push ix
        push iy

	ld ix,[ultimo_objeto_creado]

        ld e,ixl
        ld d,ixh

	;HL se pasa como par�metro
	;push hl donde esta el evento INIT, que como no se guarda en el nodo, lo tenemos que recuperar para hacer la llamada
	/*inc hl
	inc hl*/
        inc de
        inc de  ;saltamos el hueco SIGUIENTE

	;nombre del objeto
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	
	;eventos init, mov y hit
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi

        ld [ix+ACTOR.switches],0
        ld [ix+ACTOR.pat],0
        ld [ix+ACTOR.left],0
        ld [ix+ACTOR.top],0
        ld [ix+ACTOR.pvida],0
        ld [ix+ACTOR.num_sprites],0
	;llamamos al m�todo INIT del objeto reci�n creado

	ld l,[ix+ACTOR.EVENTO_INIT+0]
	ld h,[ix+ACTOR.EVENTO_INIT+1]

        ld a,[parametros_desde_mapa_b]
        ld b,a
        ld a,[parametros_desde_mapa_c]
        ld c,a
        ld a,[parametros_desde_mapa_d]
        ld d,a
        ld a,[parametros_desde_mapa_e]
        ld e,a
        ld a,[parametros_desde_mapa_a]

	call llamada_funcion_hl
	call solicita_slot_sprites ;si el objeto tiene obj_num_sprites>0, le asignamos un slot
	jr z,.hay_sprites_disponibles
	;si no hay sprites disponibles, marcamos el objeto para eliminar
	;la funci�n al princpio comprueba si obj_num_sprites==0, en ese caso sale sin probar nada
	;la comprobaci�n tambi�n funciona en ese caso, al salir con Z
	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
.hay_sprites_disponibles:	
	ld hl,num_objetos
	inc [hl]
	pop iy
	pop ix

	ret
	

llamada_funcion_hl:
	jp [hl]
	;RET se debe llamar desde cualquier funci�n llamada con este m�todo

inserta_sprites_vacios:
;Cuando se elimina un nodo, se insertan tantos sprites vacios como sprites tenga el objeto.
;como se llama dentro de recorre_nodos_eliminar, que se ejecuta despu�s de pintar
;los �ltimos sprites aparecer�n deshabilitados
;as� que no quedar� basura en pantalla
	ld a,[sprites_frame_actual]
	ld b,a
	ld a,[sprites_frame_anterior]
	sub b
	jr c,.nada_a_eliminar
	jr z,.nada_a_eliminar
	
	ld b,a
	di
.otro_sprite_vacio:
	ld a,0xd4
	out [0x60],a
	xor a
	out [0x60],a
	out [0x60],a
	out [0x60],a
	djnz .otro_sprite_vacio
	ei
.nada_a_eliminar:
	ld a,[sprites_frame_actual]
	ld [sprites_frame_anterior],a
        ret



dibuja_sprites_nodo:
;Cada nodo tiene hasta 5 sprites
;los datos de sprites del nodo tiene la informaci�n base de cada sprite
;Desp Y
;Desp X
;Pat
;NO SE USA

;actualizar las coordenadas no supone mucho gasto, y como con el SCROLL ARTRAG hay que mover los sprites si se mueve el scroll
;no nos complicamos y actualizamos siempre las coordenadas
;en el caso de cambiar el PAT, por comparaci�n con OLD_PAR se actualiza la tabla de colores

;Esta funci�n suma las propiedades del objeto Y,X y PAT a las propiedades Y,X y PAT de cada sprite del nodo
;y vuelca los valores directamente en la SPAT, y en la TABLA DE COLORES (si hay modificaci�n)

	ld a,[ix+ACTOR.num_sprites]
	or a
	ret z

	bit 7,a
	jr z,.sprites_en_objeto
.sprites_fuera_de_objeto:	
	ld l,[ix+ACTOR.sprite1_y]
	ld h,[ix+ACTOR.sprite1_y+1]
	and 01111111b ;eliminamos el bit de SPRITES FUERA DE OBJETO
	jr .direccion_sprites_seleccionada
.sprites_en_objeto:	
	push ix
	pop hl ;HL apunta al inicio del objeto
	ld de,ACTOR.sprite1_y ;lo que ya habiamos sumado antes, menos los 4 del ldir
	add hl,de
.direccion_sprites_seleccionada:	

	ld b,a ;obj_num_sprites

	ld e,[ix+ACTOR.y]
	ld c,[ix+ACTOR.x]
	ld d,[ix+ACTOR.pat]

	exx
	ld l,[ix+ACTOR.base_spat]
	ld h,[ix+ACTOR.base_spat+1]
	ld c,[ix+ACTOR.col]
	
	exx

.otro_sprite_del_nodo:

	;B :obj_num_sprites
	;E :Atributo del objeto Y
	;C :Atributo del objeto X
	;D :Atributo del objeto PAT
	;C' :Atributo del objeto COL
	;HL :Atributos de los subsprites

        ;parametro Y
	ld a,e ;E :Atributo del objeto Y
	add a,[hl]
	inc hl
	exx
	call wrtvrm
	inc hl
	exx


        ;parametro X
	ld a,c ;C :Atributo del objeto X
	add a,[hl]
	inc hl
	exx
	call wrtvrm
	inc hl
	exx

        ;parametro PAT
	ld a,d ;[D :Atributo del objeto PAT
	add a,[hl]
	inc hl
	exx
	call wrtvrm
	inc hl
	ld a,c ;c', atributo del objeto COL
	exx
	add a,[hl]
	inc hl
	exx
	call wrtvrm
	inc hl
	exx
	

	
	djnz .otro_sprite_del_nodo


	ret





comprueba_colisiones:
;recorremos num_objetos_pasivos para cada num_objetos_activos.
;En caso de colisi�n, se ejecuta el evento hit de ambos
        ;si no hay objetos_pasivos
        ld a,[num_objetos_pasivos]
        or a
        ret z
        ;o activos, nos vamos
        ld a,[num_objetos_activos]
        or a
        ret z

	ld hl,[indice_objetos_activos]
	dec hl
	dec hl
	ld [indice_objetos_activos],hl

	ld hl,[indice_objetos_pasivos]
	dec hl
	dec hl
	ld [indice_objetos_pasivos],hl


.siguiente_objeto_activo:

	ld a,[num_objetos_activos]
	dec a
	ld [num_objetos_activos],a
	inc a
	ret z


	ld hl,[indice_objetos_activos]
	ld a,[hl]
	ld ixl,a
	inc hl
	ld a,[hl]
	ld ixh,a
        dec hl
        dec hl
        dec hl
	ld [indice_objetos_activos],hl

	ld hl,[indice_objetos_pasivos]
	ld [indice_objetos_pasivos_bucle],hl
        ld a,[num_objetos_pasivos]
        ld [num_objetos_pasivos_bucle],a


.nocolisionan:
.siguiente_objeto_pasivo:
.obj_ix_marcado_para_eliminar:
.obj_iy_marcado_para_eliminar:

	ld a,[num_objetos_pasivos_bucle]
	dec a
	ld [num_objetos_pasivos_bucle],a
	inc a
	jr z,.siguiente_objeto_activo

	ld hl,[indice_objetos_pasivos_bucle]

	ld a,[hl]
	ld iyl,a
	inc hl
	ld a,[hl]
	ld iyh,a
        dec hl
        dec hl
        dec hl

	ld [indice_objetos_pasivos_bucle],hl

        ;si alguno de los objetos est� marcado para eliminar, no comprobamos
	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
	jr nz,.obj_ix_marcado_para_eliminar ;si a lo largo de la comprobacion de colisiones un objeto se marca para eliminar, no se debe tener en cuenta para el resto de comprobaciones.
                                           ;esto generaba que un objeto ya eliminado, se marcara para eliminar de nuevo. No tendr�a efecto si no tuviera c�digo asociado (por ejemplo,
                                           ;decrementar un valor del padre, como el obj_grupo_murcielagos. Si se dispara a un murcielago con boomerangs y se mata a la vez con uno que sube y
                                           ;otro que vuelve, se produce este error.


	bit SWITCHES_ACTOR.ELIMINAR,[iy+ACTOR.switches]
	jr nz,.obj_iy_marcado_para_eliminar ;si a lo largo de la comprobacion de colisiones un objeto se marca para eliminar, no se debe tener en cuenta para el resto de comprobaciones.
                                           ;esto generaba que un objeto ya eliminado, se marcara para eliminar de nuevo. No tendr�a efecto si no tuviera c�digo asociado (por ejemplo,
                                           ;decrementar un valor del padre, como el obj_grupo_murcielagos. Si se dispara a un murcielago con boomerangs y se mata a la vez con uno que sube y
                                           ;otro que vuelve, se produce este error.

	ld l,[iy+ACTOR.x]
        ld h,0
        ld [aux],hl
        ld e,[iy+ACTOR.left] ;left siempre es positivo
        ld d,0
        add hl,de
	ex de,hl

        ld l,[ix+ACTOR.x]
        ld h,0
        ld [aux2],hl
        ld a,[ix+ACTOR.left]
        add a,[ix+ACTOR.width]
        ld c,a
        ld b,0
	add hl,bc        

	call compara_hl_de
        jr c,.nocolisionan

        ld hl,[aux2]
        ld e,[ix+ACTOR.left]
        ld d,0
        add hl,de
	ex de,hl

        ld hl,[aux]
        ld a,[iy+ACTOR.left]
        add a,[iy+ACTOR.width]
        ld c,a
        ld b,0
        add hl,bc
	call compara_hl_de
        jr c,.nocolisionan


        ld l,[iy+ACTOR.y]
        ld h,0
        ld [aux],hl
        ld e,[iy+ACTOR.top] 
        etodeextendsign ;IY representa a los pasivos: Solo los desplazadores pueden tener un top negativo, por eso esto se queda as�
        add hl,de
	ex de,hl        

        ld l,[ix+ACTOR.y]
        ld h,0
        ld [aux2],hl
        ld a,[ix+ACTOR.top]
        add a,[ix+ACTOR.height]
        ld c,a
        ld b,0
        add hl,bc
	call compara_hl_de
        jp c,.nocolisionan



        ld hl,[aux2]
        ld e,[ix+ACTOR.top]
        ld d,0
        add hl,de
        ex de,hl

        ld hl,[aux]
        ld a,[iy+ACTOR.top]
        add a,[iy+ACTOR.height]
        ld c,a
        ctobcextendsign
        add hl,bc
	call compara_hl_de
        jp c,.nocolisionan


.colisionan:
        ;al entrar aqu�, IX es el objeto Activo y IX el pasivo
        ;IX es THIS y IY es ELOTRO
	ld l,[ix+ACTOR.EVENTO_HIT]
	ld h,[ix+ACTOR.EVENTO_HIT+1]



	ld [nodo_padre],ix ;cuando un objeto muere y crea objetos (explosiones,llamas), se utiliza esta variable como PADRE GENERADOR
	;estas comprobaciones se hacen fuera del ciclo normal de creaciones/destrucciones (ciclo MOV), as� que no hay que restaurarlo, ya se restaurar� en el siguiente ciclo


	call llamada_funcion_hl


        push ix



        ;intercambiamos IX e IY
        push ix
        push iy
        pop ix
        pop iy

        ;los objetos siempre se refieren a s� mismos como IX
        ;IX es THIS y IY es ELOTRO
	ld l,[ix+ACTOR.EVENTO_HIT]
	ld h,[ix+ACTOR.EVENTO_HIT+1]




	ld [nodo_padre],ix ;cuando un objeto muere y crea objetos (explosiones,llamas), se utiliza esta variable como PADRE GENERADOR
	;estas comprobaciones se hacen fuera del ciclo normal de creaciones/destrucciones (ciclo MOV), as� que no hay que restaurarlo, ya se restaurar� en el siguiente ciclo




	call llamada_funcion_hl

        pop ix


	jp .siguiente_objeto_pasivo

	ret


solicita_slot_sprites:
;esta funci�n revisa los slots libres en funci�n a obj_num_sprites
	ld a,[ix+ACTOR.num_sprites]
	or a
	ret z
	dec a
	jr z,.un_sprite
	dec a
	jr z,.dos_sprites
	sub 2
	jr z,.cuatro_sprites
	jr $ ;si no es ninguno de los anteriores, bloqueo, para revisar en tiempo de desarrollo
	
.cuatro_sprites:	
	ld hl,slots_sprites_libres_x4
	ld a,[hl]
	exx
	ld de,4*4
	ld b,cuantos_megasprites_de_4
	ld c,1 ;se usa para la m�scara, para establecer el bit y ocupar el slot, y para guardarlo en el objeto y resetearlo al eliminarlo
	ld hl,0x1b00
	jr .busca_slot_libre
	
.dos_sprites:
	ld hl,slots_sprites_libres_x2
	ld a,[hl]
	exx
	ld de,4*2
	ld b,cuantos_megasprites_de_2
	ld c,1 ;se usa para la m�scara, para establecer el bit y ocupar el slot, y para guardarlo en el objeto y resetearlo al eliminarlo
	ld hl,0x1b50
	jr .busca_slot_libre
	
.un_sprite:	
	ld hl,slots_sprites_libres_x1
	ld a,[hl]
	exx
	ld de,4
	ld b,cuantos_megasprites_de_1
	ld c,1 ;se usa para la m�scara, para establecer el bit y ocupar el slot, y para guardarlo en el objeto y resetearlo al eliminarlo
	ld hl,0x1b70
	;jr .busca_slot_libre
.busca_slot_libre:
	srl a
	jr nc,.libre
	sla c ;movemos la m�scara hacia la izquierda, ya que el bit est� ocupado
	add hl,de ;avanzamos el puntero del SPAT, en el n�mero de bytes que ocupan los N sprites solicitados
	djnz .busca_slot_libre
	xor a
	inc a
	;slot de sprite no disponible
	ret
	
	
.libre:
	exx ;HL' tiene la direcci�n de control del slot
	ld a,[hl]
	exx
	or c
	ld [ix+ACTOR.mascara_slot_sprite_usado],c
	exx
	ld [hl],a ;marcamos el bit del slot reci�n asignado
	ld [ix+ACTOR.slot_sprite_usado],l
	ld [ix+ACTOR.slot_sprite_usado+1],h
	exx
	ld [ix+ACTOR.base_spat],l
	ld [ix+ACTOR.base_spat+1],h
	xor a
	ret
	
libera_slot_sprites:
	ld a,[ix+ACTOR.num_sprites]
	or a
	ret z
	;liberamos el slot
	ld l,[ix+ACTOR.slot_sprite_usado]
	ld h,[ix+ACTOR.slot_sprite_usado+1]
	ld a,[ix+ACTOR.mascara_slot_sprite_usado]
	cpl
	ld c,a
	ld a,[hl]
	and c
	ld [hl],a
	
	;ocultamos los sprites del slot
	
	ld b,[ix+ACTOR.num_sprites]
	ld de,4
	ld l,[ix+ACTOR.base_spat]
	ld h,[ix+ACTOR.base_spat+1]
.otro_sprite:	
	ld a,217
	call wrtvrm
	add hl,de
	djnz .otro_sprite
	ret
		
	


;***************************************************************************************************************************************************
;*   Esta rutina sirve para recorrer los nodos de los objetos
;***************************************************************************************************************************************************
recorrer_nodos:


.nuevo_ciclo:
	ld ix,[nodos_ocupados]
        ld a,ixh
        inc a;cp 0xff
        ret z

	ld hl,objetos_pasivos
	ld (indice_objetos_pasivos),hl
	ld hl,objetos_activos
	ld (indice_objetos_activos),hl

	ld hl,0
	ld [num_objetos_activos],hl ;num_objetos_activos,num_objetos_pasivos


	ld hl,0xffff
	ld [nodo_anterior],hl
.nuevo_nodo:
	ld [nodo_anterior_aux],ix
	
	


	;ejecutamos metodo_exec
	ld [nodo_padre],ix


	;se comprueba el bit de eliminacion antes de ejecutar el objeto, para el caso en el que se haya activado dentro de COMPRUEBA_COLISIONES
	;que esta fuera de este ciclo y se ejecuta despu�s, pero los efectos quedan para el siguiente ciclo. Ej:
	;Ciclo 1 objeto en cuesti�n con el bit de eliminacion apagado
	;comprobar colisiones: se activa el bit, PERO NO SE ELIMINA
	;Ciclo 2 El bit de eliminaci�n est� activado. Sin este control, habr�a que controlar el caso en cada objeto
	;para evitar probleas caso hijo->padre.hijos--, se controla desde aqu�
	;Se podr�a eliminar el nodo dentro de la misma rutina COMPRUEBA_COLISIONES, pero mejor hacerlo solo en un sitio
	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
	jp nz,.objeto_eliminado_durante_comprueba_colisiones

	
	; ; ld l,[ix+ACTOR.EVENTO_MOV]
	; ; ld h,[ix+ACTOR.EVENTO_MOV+1]
	; ; call llamada_funcion_hl

	; bit SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches] ;obj_XXXX_eliminar
	; jp nz,.noeliminar

	; set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;Elimino al actor

.noeliminar

	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
.objeto_eliminado_durante_comprueba_colisiones:
	ld l,[ix+ACTOR.NODO_SIGUIENTE]
	ld h,[ix+ACTOR.NODO_SIGUIENTE+1]
	push hl
	jr nz,.liberamos_nodo
.objeto_dentro_de_pantalla:

; call dibuja_sprites_nodo
	
.objeto_fuera_pantalla:	
.sprites_fuera_pantalla_eliminados:	
.no_hay_sprites_que_eliminar_fuera_pantalla:	
	;solo cambiamos el nodo anterior cuando no eliminamos. Si eliminamos, el anterior sigue siendo el mismo (no el que se ha eliminado)
	ld hl,[nodo_anterior_aux]
	ld [nodo_anterior],hl
	jr .no_liberamos_nodo
.liberamos_nodo:
	call libera_slot_sprites
        call libera_nodo
        jr .nodo_liberado
.no_liberamos_nodo:
	bit SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	jr z,.no_es_activo
.es_activo:
	ld hl,num_objetos_activos
	inc [hl]
	ld hl,[indice_objetos_activos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_activos],hl
	jr .no_es_pasivo
.no_es_activo:
	bit SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	jr z,.no_es_pasivo
.es_pasivo:
	ld hl,num_objetos_pasivos
	inc [hl]
	ld hl,[indice_objetos_pasivos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_pasivos],hl

.no_es_pasivo:
.objeto_fuera_de_pantalla_es_ignorado:

.nodo_liberado:

	pop hl
	ld b,l
	ld a,h
	cp 0xff
	jr z,.acabamos

	
	
	ld ixh,a
	ld ixl,b

	jp .nuevo_nodo

.acabamos:

	ret


	
;***************************************************************************************************************************************************
;*   Esta rutina sirve para recorrer los nodos pasivos y los elimina
;***************************************************************************************************************************************************
eliminar_actores_pasivos_si_procede:

	ld a,(eliminar_actores_pasivos)
	cp 0
	ret z;Me salto si el número de proyectiles es 0(para probar)

.nuevo_ciclo:
	ld ix,[nodos_ocupados]
        ld a,ixh
        inc a;cp 0xff
        ret z

	ld hl,objetos_pasivos
	ld (indice_objetos_pasivos),hl
	ld hl,objetos_activos
	ld (indice_objetos_activos),hl

	ld hl,0
	ld [num_objetos_activos],hl ;num_objetos_activos,num_objetos_pasivos


	ld hl,0xffff
	ld [nodo_anterior],hl
.nuevo_nodo:
	ld [nodo_anterior_aux],ix
	
	


	;ejecutamos metodo_exec
	ld [nodo_padre],ix


	;se comprueba el bit de eliminacion antes de ejecutar el objeto, para el caso en el que se haya activado dentro de COMPRUEBA_COLISIONES
	;que esta fuera de este ciclo y se ejecuta despu�s, pero los efectos quedan para el siguiente ciclo. Ej:
	;Ciclo 1 objeto en cuesti�n con el bit de eliminacion apagado
	;comprobar colisiones: se activa el bit, PERO NO SE ELIMINA
	;Ciclo 2 El bit de eliminaci�n est� activado. Sin este control, habr�a que controlar el caso en cada objeto
	;para evitar probleas caso hijo->padre.hijos--, se controla desde aqu�
	;Se podr�a eliminar el nodo dentro de la misma rutina COMPRUEBA_COLISIONES, pero mejor hacerlo solo en un sitio
	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
	jp nz,.objeto_eliminado_durante_comprueba_colisiones

	
	; ld l,[ix+ACTOR.EVENTO_MOV]
	; ld h,[ix+ACTOR.EVENTO_MOV+1]
	; call llamada_funcion_hl

	bit SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches] ;obj_XXXX_eliminar
	jp nz,.noeliminar

	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;Elimino al actor

.noeliminar

	bit SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches] ;obj_XXXX_eliminar
.objeto_eliminado_durante_comprueba_colisiones:
	ld l,[ix+ACTOR.NODO_SIGUIENTE]
	ld h,[ix+ACTOR.NODO_SIGUIENTE+1]
	push hl
	jr nz,.liberamos_nodo
.objeto_dentro_de_pantalla:

; call dibuja_sprites_nodo
	
.objeto_fuera_pantalla:	
.sprites_fuera_pantalla_eliminados:	
.no_hay_sprites_que_eliminar_fuera_pantalla:	
	;solo cambiamos el nodo anterior cuando no eliminamos. Si eliminamos, el anterior sigue siendo el mismo (no el que se ha eliminado)
	ld hl,[nodo_anterior_aux]
	ld [nodo_anterior],hl
	jr .no_liberamos_nodo
.liberamos_nodo:
	call libera_slot_sprites
        call libera_nodo
        jr .nodo_liberado
.no_liberamos_nodo:
	bit SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	jr z,.no_es_activo
.es_activo:
	ld hl,num_objetos_activos
	inc [hl]
	ld hl,[indice_objetos_activos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_activos],hl
	jr .no_es_pasivo
.no_es_activo:
	bit SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]
	jr z,.no_es_pasivo
.es_pasivo:
	ld hl,num_objetos_pasivos
	inc [hl]
	ld hl,[indice_objetos_pasivos]
	ld a,ixl
	ld [hl],a
	inc hl
	ld a,ixh
	ld [hl],a
	inc hl
	ld [indice_objetos_pasivos],hl

.no_es_pasivo:
.objeto_fuera_de_pantalla_es_ignorado:

.nodo_liberado:

	pop hl
	ld b,l
	ld a,h
	cp 0xff
	jr z,.acabamos

	
	
	ld ixh,a
	ld ixl,b

	jp .nuevo_nodo

.acabamos:

	ret