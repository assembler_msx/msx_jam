TILE_PARED EQU 16; #E1
TILE_PROYECTIL EQU 17;#0F

;Esta rutina modifica un tile en el buffer
;input
; REgistro hlNumero de tile a modificar
; Registro a Nuevo valor
modificarTile:
	push hl
	ld de,buffer_pantalla

	add hl,de;Me situo en la posicicón del modificarTile



;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
;REVISAR*******************************
	push af
	ld b,a; Paso el registro a b
	ld a,(hl);Miro que tile hay actualmente
	cp b
	pop af

	


	ld (hl),a;Copio el tile

	pop hl


	ret z;Me salto si el valor anterior era igual

	;ahora lo copio en la VRAM

	ld  de,#1800
	add hl,de;Me situo en la posicicón del modificarTile
	

	ld b,a;Guardo en b el valor

		;Ya tengo en hl el byte buscado, ahora solo falta desplazarme en la horizontal
;di; DESHABILITO LAS INTERRUPCIONES
	in		a,(#99)						;Sincronizar el VDP
	ld		a,l							;Cargo la dirección de la tile (el byte bajo)
[2]	nop
	out		(#99),a						;Mango al VDP el byte bajo
	ld		a,h							;El byte bajo siempre es 0 (HAY QWUE SUMAR SIEMPRE 64 AL ESCRIBIR EN VRAM EN EL BYTE ALTO)
	add		a,64
	;Retardo para que haya, como mínimo 17 ciclos entre dos outs
	nop
	out		(#99),a						;Mando al VDP el byte alto
	ld a,b;REcupero el valor
	;Retardo para que haya, como mínimo 17 ciclos entre dos outs
[4]	nop
	out		(#98),a						

	ret



;************************************************************************************************************************

actualizar_marcador_cargador
	ld a,(num_proyectiles)
	ld c,a;Almaceno el número de partidas en c

	ld b,0
	ld hl, 2
.loop15Tiles
	push bc
	inc b;Lo incremento para que luego en la comparación con el número de proyectiles me ponga un tipo ...
	ld d,TILE_PROYECTIL
	ld a,c;REcupero en a el número de proyectiles
	cp b

	jr. nc,.esTileProyectil
	ld d,TILE_PARED
.esTileProyectil
	push hl
	ld a,d
	call modificarTile
	pop hl

	INC HL


	pop bc
	inc b
	ld a,b
	cp 10
	jr nz,.loop15Tiles	

	ret
	