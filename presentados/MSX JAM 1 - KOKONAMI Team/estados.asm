

inicializa_estado:
	call inicializa_nodos ;inicializa la zona de memoria que alojará a los objetos

	
	ld a,[estado_actual]
	call JumpIndex
	dw logo_inicializa
	dw menu_inicializa
	dw juego_inicializa





	
logo_inicializa:
	call carga_graficos_logo
	
	ld hl,class_logo
	call inserta_objeto
	ret




menu_inicializa:
	call carga_graficos_menu
	
	ld hl,class_menu
	call inserta_objeto
	ret


;;;;; DGA
cargar_enemigos:
	; enemy 1 - bola
	; ld hl,class_bola		;insertamos el objeto bola
	; ld c,80
	; ld b,104
	; call inserta_objeto
		
	; ; enemy 2 - bola
	; ld hl,class_bola		;insertamos el objeto bola
	; ld c,70
	; ld b,80
	; call inserta_objeto

	; ; enemy 3 - bola
	; ld hl,class_cargador		;insertamos el objeto bola
	; ld c,190
	; ld b,60
	; call inserta_objeto

	; enemy 4 - Proyectil
	; ld hl,class_proyectil		;insertamos el objeto bola
	; ld b,2 ;Velocidad X
	; ld b,0 ;Velocidad Y
	; call inserta_objeto

	;ld a,3
	;ld [numero_enemigos_en_pantalla],a

	ret
;;;;; DGA



juego_inicializa:


	LD A,(JIFFY);MSX BIOS time variable
	ld [generate_rnd_number], a	
	
	call carga_graficos
	
	ld hl,mapeado
	ld [puntero_mapa_stage],hl ;este puntero guarda la dirección en ROM de la esquina superior izquierda del mapa
	call dibuja_mapa

	
	
	ld hl,class_prota		;insertamos el objeto prota
	call inserta_objeto

	; Cargamos enemigos - DGA
	call cargar_enemigos	

	call actualizar_marcador_cargador


	;Valores iniciales de disparo
	ld a,VELOCIDAD_PROYECTIL;Velocidad X
	ld (ultima_direccion_disparo),a;
	xor a;Velocidad Y
	ld (ultima_direccion_disparo+1),a;

		
	
	; ld hl,class_ejemplo	;insertamos el objeto ejemplo
	; ld c,180
	; ld b,10
	; call inserta_objeto

	ld hl,class_cargador	;insertamos el objeto ejemplo
	ld c,50
	ld b,100
	call inserta_objeto

	ld hl,class_cargador	;insertamos UN CARGADOR DE ejemplo
	ld c,50
	ld b,150
	call inserta_objeto


	call enascr ;habilitamos la pantalla
	
	ld hl,musica1
	call PT3_INIT ;inicializamos el player con la música apuntada por HL
	ld hl,sonidos
	call ayFX_SETUP ;inicializamos el reproductor de sonidos con el banco de sonidos apuntado por HL
	ret
	
	