Un miércoles cualquiera, de un mes cualquiera, de un año cualquiera, en un lugar cualquiera en medio de un bosque cualquiera,
el grupo de amigos (y amigas) quedais para pasar unos días en una vieja mansión, en plan refugio rural, muy de moda.

Al acercaros a la mansion, una densa niebla se levanta... de repente te das cuenta de que no ves ni oyes a ninguno de tus amigos,
ni siquiera a ninguna de tus amigas, lo cual es mucho más perturbador.

Al despertarte, te encuentras en una habitación decorada con espadas y otros elementos antiguos. Parece que el encargado de la limpieza
hace mucho que no pasa el cepillo por aquí. Menos mal que siempre llevas encima el ventolín, porque con este polvo lo vas a necesitar.
Decidido a encontrar al restro del grupo, y a salir de la mansión (bueno, al revés), deberás explorar las habitaciones y desentrañar el
misterio de la vieja mansión. No te demores, porque no te queda demasiado ventolín y podrías asfixiarte recorriendo estas asfixiantes
y mugrientas habitaciones... Además, hoy televisan el partido de la Champions y no te lo puedes perder! (y además habías quedado con
tu chica... o era mañana?)

Mucha suerte, parece que la vas a necesitar...



EL GRUPO 1
