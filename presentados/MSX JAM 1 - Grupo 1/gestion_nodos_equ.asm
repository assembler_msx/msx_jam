MAX_SPRITES_SW	equ 16








;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------
;DEFINICION DE EQUs PARA OBJETOS
;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------
;---------------------------------------------------------------------------------------------------------------------------------

NODOS_SIZE 			equ 80
NODOS_MAX 			equ 4096/NODOS_SIZE


	STRUCT SWITCHES_ACTOR
		;se utilizan tipos de dato byte, pero se utilizan como posiciones de bit dentro de SWITCHES_ACTOR

PROTA 				byte 	;si es 1, el objeto es el prota, si no, pues nada
ELIMINAR 			byte 	;si se activa, al salir del MOV del objeto, será eliminado
LIBRE2 				byte
ACTIVO 				byte 	;los objetos activos son el prota y sus balas
PASIVO 				byte 	;los objetos pasivos son los enemigos, sus balas, y los objetos que se pueden recoger
LIBRE5				byte
LIBRE6				byte
PAT_MODIFICADO		byte 	;el objeto debe activar este bit si modifica el PAT, para que actualiza_sprites_nodo actualice el PAT y los colores
	ENDS

	
	STRUCT ACTOR
NODO_SIGUIENTE		word 	;puntero al siguiente nodo de la lista de objetos
NOMBRE_OBJETO		ds 8 	;almacena el nombre del objeto, si depurando_nombres es 1, para "ver" en el depurador el objeto actual
EVENTO_INIT 		word 	;puntero a la funcion INIT del objeto. Se almacena, pero solo se usa una vez, al crear el objeto
EVENTO_MOV 			word 	;puntero a la funcion MOV del objeto. Se ejecuta en cada frame
EVENTO_HIT 			word 	;puntero a la funcion HIT del objeto. Se llama cada vez que dos objetos colisionan
				     		;cuando colisionan, se llama al evento HIT del objeto ACTIVO y luego al del PASIVO
				     		;dependiendo de la lógica de cada objeto, el comportamiento ante la colisión podrá estar
				     		;en uno solo de ellos, o en los dos
				     		;Siempre IX hace referencia al objeto actual, y IY "al otro"
y             		byte 	;coordenada Y base del objeto. Los sprites podrán estar desplazados respecto a esta
x                   byte 	;coordenada X base del objeto. Los sprites podrán estar desplazados respecto a esta
pat             	byte 	;Pattern base del objeto.
col	             	byte 	;color base del objeto

mascara_slot_sprite_usado	byte 	;cuando se solicita (automaticamente a través de inserta_objeto) un número de sprites
				     				;aquí se guarda el bit que indica la posición del slot que se le ha asignado
				     				;no hace falta tratar con él fuera del motor

width				byte	;anchura del cuadro de colisión del objeto
height         		byte	;altura del cuadro de colisión del objeto
top                 byte	;desplazamiento desde la Y para comprobar colisiones
left                byte	;desplazamiento desde la X para comprobar colisiones
switches 			byte	;los bits están definidos en las etiquetas SW_OBJETO_xxx

pvida               byte	;puntos de vida del objeto
base_spat	      	word	;almacena la dirección en la que están los atributos del primer sprite, los siguientes
							;obj_num_sprites serán siempre correlativos.
							;Se calcula la posición con solicita_slot_sprites
							;en función del numero de sprites que tiene el objeto
	
slot_sprite_usado 		word	;almacena la dirección del byte donde se controla el slot de sprites
								;cuando se libera el objeto, se deja libre el slot mediante ese byte y obj_mascara_slot_sprite_usado

num_sprites	      		byte	;número de sprites del objeto

sprite1_y	      		byte	;incremento de Y respecto a ACTOR.y del sprite del color 1
sprite1_x	      		byte	;incremento de x respecto a ACTOR.x del sprite del color 1
sprite1_pat	      		byte	;incremento de PAT respecto a ACTOR.pat del sprite del color 1
sprite1_col      		byte	;incremento de COL respecto a ACTOR.col del sprite del color 2

sprite2_y	      		byte	;incremento de Y respecto a ACTOR.y del sprite del color 2
sprite2_x	      		byte    ;incremento de x respecto a ACTOR.x del sprite del color 2
sprite2_pat	      		byte    ;incremento de PAT respecto a ACTOR.pat del sprite del color 2
sprite2_col      		byte    ;incremento de COL respecto a ACTOR.col del sprite del color 2

sprite3_y	      		byte	;incremento de Y respecto a ACTOR.y del sprite del color 3
sprite3_x	      		byte    ;incremento de x respecto a ACTOR.x del sprite del color 3
sprite3_pat	      		byte    ;incremento de PAT respecto a ACTOR.pat del sprite del color 3
sprite3_col      		byte    ;incremento de COL respecto a ACTOR.col del sprite del color 3

sprite4_y	      		byte	;incremento de Y respecto a ACTOR.y del sprite del color 4
sprite4_x	      		byte    ;incremento de x respecto a ACTOR.x del sprite del color 4
sprite4_pat	      		byte    ;incremento de PAT respecto a ACTOR.pat del sprite del color 4
sprite4_col      		byte    ;incremento de COL respecto a ACTOR.col del sprite del color 4

variables          		byte 	;se utiliza para controlar el offset de los structs de los objetos, 
					;colocando en la definicion STRUCT NOMBRE_STRUCT,ACTOR.variables
	ENDS



	