macro color_borde color
	ld b,color
	ld c,7
	call 0x0047 ;wrtvdp
endmacro


macro selecciona_segmento_4_ram
	out [0xfd],a
endmacro
macro selecciona_segmento_8_ram
	out [0xfe],a
endmacro
macro selecciona_segmento_c_ram
	out [0xff],a
endmacro

macro selecciona_segmento_4_megarom
	ld [0x5000],a
endmacro

macro selecciona_segmento_6_megarom
	ld [0x7000],a
endmacro

macro selecciona_segmento_8_megarom
	ld [0x9000],a
endmacro

macro selecciona_segmento_a_megarom
	ld [0xb000],a
endmacro

; sign-extends e into de
macro etodeextendsign
	ld a, e
	rlca		; move sign bit into carry flag
	sbc a, a	; a is now 0 or 11111111, depending on the carry
	ld d, a
endmacro

macro ctobcextendsign
	ld a, c
	rlca		; move sign bit into carry flag
	sbc a, a	; a is now 0 or 11111111, depending on the carry
	ld b, a
endmacro
macro ltohlextendsign
	ld a, l
	rlca		; move sign bit into carry flag
	sbc a, a	; a is now 0 or 11111111, depending on the carry
	ld h, a
endmacro

;add A to HL	
macro ADD_HL_A
	add	a,l
	ld	l,a
	jr	nc,$+3
	inc	h
endmacro	
;add A to HL	
macro ADD_DE_A
	add	a,e
	ld	e,a
	jr	nc,$+3
	inc	d
endmacro	
						
;mi código : assembler code

macro LEFT_DEHL
;rotate DEHL left (de msb, hl lsb)
    sla l
    rl h
    rl e
    rl d
endmacro
;rotate HL left
macro LEFT_HL 
    add hl,hl
endmacro
    
;rotate DE left
macro LEFT_DE 
    sla e
    rl d
endmacro
    
;rotate BC left
macro LEFT_BC 
    sla c
    rl b
endmacro
    

;rotate HL right
macro RIGHT_HL 
	srl h
	rr l
endmacro
    
;rotate DE right
macro RIGHT_DE 
    srl d
    rr e
endmacro

macro RIGHT_DE_SIGNO ;mantiene el signo 
    sra d
    rr e
endmacro
    
    
;rotate BC right
macro RIGHT_BC 
    srl b
    rr c
endmacro

macro macro_nombre_objeto nombre
	
	if depurando_nombres=1
		db nombre
	else
		db '        '
	endif

endmacro
