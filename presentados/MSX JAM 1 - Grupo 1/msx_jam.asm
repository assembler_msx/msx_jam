depurando_nombres equ 1




	output "msx_jam.rom"
	
	
	
       	include "macros.asm"
	include "defpages.asm"
	include "equ.asm"






	page 0
	

        org 0x4000
	db "AB"
	dw inicio
	db 0,0,0,0,0,0,0,0,0,0
	db 0,0
cabecera:
	db "AS-R1807"
inicio:

	di
	call disscr ;apaga la pantalla

	call inicializa_modo_grafico 			;todo el tiempo estaremos en modo SC2 con sprites de 16x16
	call resetea_memoria				;borra toda la memoria utilizada por el juego
	call busca_slotset				;localiza la segunda mitad de la ROM colocando el mismo slot en la dirección 0x8000
	ld hl,interrupcion				
	call inicializa_interrupcion 			;activamos la interrupción, que es común a todos los estados del juego

	xor a						;inicializamos el estado (en estados.asm está la función que llama 
	ld [estado_actual],a				;a la inicialización de los diferentes estados en función de esta variable
	ld [subestado_actual],a				;esta variabla aún no se usa
	
.salimos_del_bucle_principal:
	di
	call disscr 					;apaga la pantalla
	call clrspr 					;inicializa los sprites
	call apaga_musica				;apaga la música si algún estado anterior la ha activado
	call inicializa_estado				;inicializa el estado actual. Cada estado es el encargado de pasar al siguiente estado
	
	call enascr 					;enciende la pantalla
	
	ld a,1						;es el interruptor que indica si debemos seguir en el bucle principal, o salir para
	ld [estado_juego],a				;revisar un cambio de estado
	
.bucle:

	ei
	ld a,[interrupcion_ejecutada] 			;esperamos a que salte la INTERRUPCION VBLANK para comenzar la ejecución de 1 frame
	or a
	jr z,.bucle
.empezamos_proceso:

	xor a
	ld [interrupcion_ejecutada],a			;reseteamos el switch de control


	call lee_teclado 				;cada frame se ejecuta 1 sola vez la lectura del teclado
							;las variables buffer_teclado0..buffer_teclado8 tienen el mapa de teclado 
							;con todas las teclas
							;si el bit de una tecla está a 0, está pulsada, si está a 1, no está pulsada
							;las teclas que van a cada variable se ven en el fichero funciones.asm, función lee_teclado
	call procesa_nodos 				;ejecuta todos los objetos que se han insertado en la inicialización del estado
	call comprueba_colisiones			;comprueba colisiones entre todos los objetos SWITCH_ACTOR.ACTIVO y
							;SWITCH_ACTOR.PASIVO
							;el motor crea dos listas, una con los objetos SWITCH_ACTOR.ACTIVO
							;y otra con los objetos SWITCH_ACTOR.PASIVO
							;si los cuadros definidos por:
							;   ACTOR.x+ACTOR.left y ACTOR.x+ACTOR.left+ACTOR.width
							;   y
							;   ACTOR.y+ACTOR.top y ACTOR.y+ACTOR.top+ACTOR.height
							;de un objeto SWITCH_ACTOR.ACTIVO y otro SWITCH_ACTOR.PASIVO
							;coinciden, se ejecuta el evento HIT de los dos, primero el ACTIVO
							;y luego el pasivo
							;en cada llamada, IX siempre hace referencia al objeto del que se está
							;ejecutando el evento HIT, y IY al otro objeto

	ld a,[estado_juego]				;si esta variable se deja a 0, salimos del bucle principal
	or a						;el proceso que haya asignado ese valor debe cambiar la variable estado_actual
	jr z,.salimos_del_bucle_principal		;al estado correspondiente
	jr .bucle
	


	include "estados.asm"
	include "funciones.asm"
	
	include "funciones_logo.asm"
	include "funciones_menu.asm"
	include "funciones_juego.asm"
	
	include "gestion_nodos.asm"
	include "PT3-ROM_sjasm.asm"
	include "ayFX-ROM_sjasm.asm"
	
musica1:	
	incbin "musica\M-Tanks BGM1.pt3"
sonidos:	
	incbin "musica\demo.afb"
	
	include "objetos\prota.asm"
	include "objetos\bola.asm"
	include "objetos\key.asm"
	include "objetos\bottle.asm"
	include "objetos\malta.asm"
	include "objetos\ejemplo.asm"
	include "objetos\logo.asm"
	include "objetos\menu.asm"


	include "graficos\sprites.asm"
	page 1
	
	
	page 2
	include "page_2_y_3.asm"
	page 4
	include "page_4.asm"
	page 5
	include "page_5.asm"


	include "variables.asm"
	


