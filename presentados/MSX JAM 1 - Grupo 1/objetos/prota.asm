class_prota:
	macro_nombre_objeto 'prota   '
	dw class_prota_init
	dw class_prota_mov
	dw class_prota_hit


class_prota_init:
	ld [ix+ACTOR.num_sprites], 2						;Sprites con 2 colores
	
	set SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	set SWITCHES_ACTOR.PROTA,[ix+ACTOR.switches]		;Activamos el switch del ACTOR.PROTA
	
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],0*4
	ld [ix+ACTOR.sprite1_col],15
	ld [ix+ACTOR.sprite2_y],0							;Solo necesario si el sprite tiene mas de un color
	ld [ix+ACTOR.sprite2_x],0							;Los frames de cada direccion se calculan en otra parte
	ld [ix+ACTOR.sprite2_pat],1*4
	ld [ix+ACTOR.sprite2_col],1

	ld [ix+ACTOR.x], 100
	ld [ix+ACTOR.y], 104
	ld [ix+ACTOR.pat], SPRITE_PROTA_PARADO

	ld [ix+ACTOR.left], 2
	ld [ix+ACTOR.top], 2
	ld [ix+ACTOR.width], 12
	ld [ix+ACTOR.height], 12
	
	
	ld [ix+PROTA.retardo_frame_paso],1
	ld [ix+PROTA.frame_paso],0
	ld [ix+PROTA.switches],0

	ld a, [pvida_prota]
	ld [ix+ACTOR.pvida], a
	ret
	
class_prota_mov:

	ld [ix+ACTOR.num_sprites], 2
	

	call prota_gestion_andar

	call prota_gestion_pasos

	ret

class_prota_hit:
	ld a,[iy+ACTOR.pvida] 	;si el objeto con el que ha colisionado el prota tiene pvida, recibe un impacto
	or a
	ret z
	set SWITCHES_ACTOR.ELIMINAR,[iy+ACTOR.switches]
	dec [ix+ACTOR.pvida]
	jr nz,.vida_perdida
	call reset_estado
	
	ret
.vida_perdida:
	ld a,[ix+ACTOR.pvida]
	ld [pvida_prota],a
	call mismo_estado
	
	ret
	

prota_gestion_andar:
;1000  Der Aba Arr Izq Del Ins Home Space
	res SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	
	ld a,[buffer_teclado8]
	bit 7,a
	call z, .derecha
	ld a,[buffer_teclado8]
	bit 4,a
	call z, .izquierda
	ld a,[buffer_teclado8]
	bit 5,a
	call z, .arriba
	ld a,[buffer_teclado8]
	bit 6,a
	call z, .abajo

	call comprueba_salida
	
	ret
.derecha:
	ld b, 15					;COLISION TILES X
	ld c, 11					;COLISION TILES Y
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	inc [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret nz
	ld [ix+PROTA.retardo_frame_paso],1
	set SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.izquierda:
	ld b, 15
	ld c, 5
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	dec [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret z
	ld [ix+PROTA.retardo_frame_paso],1
	res SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.arriba:
	ld b, 14
	ld c, 6
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	ld b, 14
	ld c, 10
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	dec [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
.abajo:
	ld b, 16
	ld c, 6
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	ld b, 16
	ld c, 10
	call lee_caracter_objeto
	cp 128 ;la última fila de tiles del set se considera DURA
	ret nc
	inc [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
	
		
prota_gestion_pasos:
	bit SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret z
	dec [ix+PROTA.retardo_frame_paso]
	ret nz
	ld [ix+PROTA.retardo_frame_paso], 8
	
	ld a,[ix+PROTA.frame_paso]
	xor 00000001b 
	ld [ix+PROTA.frame_paso], a
	ld hl, frames_prota
	ADD_HL_A			;MACRO
	ld a, [hl]
	bit SWITCHES_PROTA.DERECHA, [ix+PROTA.switches]
	jr nz, .mirando_derecha
	;Mirando a la izquierda
	add 4*4				;Aqui se definen los frames a saltar para localizar el primer frame a la izquierda
.mirando_derecha:	
	ld [ix+ACTOR.pat], a
	
	ret
	
comprueba_salida:
	ld a,[ix+ACTOR.x]
	cp 20
	jr c,.salida_por_izquierda
	cp 256-68
	jr nc,.salida_por_derecha
	ld a,[ix+ACTOR.y]
	cp 24
	jr c,.salida_por_arriba
	cp 192-40
	jr nc,.salida_por_abajo
	ret
.salida_por_izquierda:
	ld a,256-72
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	
	ld a,1
	call ayFX_INIT
	ret
.salida_por_derecha:
	ld a,24
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret

.salida_por_arriba:
	ld a,192-48
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret
.salida_por_abajo:
	ld a,32
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret
		
;*******************************************************************************		
SPRITE_PROTA_PARADO equ 0*4

SPRITE_PROTA_ANDANDO_0 equ 0*4		;
SPRITE_PROTA_ANDANDO_1 equ 2*4		;En el original es 3*4, pq el sprite tiene 3 colores

	STRUCT SWITCHES_PROTA
	;se utilizan tipos de dato byte, pero se utilizan como posiciones de bit dentro de PROTA.switches
ANDANDO			byte
LIBRE1			byte
LIBRE2			byte
LIBRE3			byte
DERECHA			byte
LIBRE5			byte
LIBRE6			byte
LIBRE7			byte
	ENDS

frames_prota:
	db SPRITE_PROTA_ANDANDO_0
	db SPRITE_PROTA_ANDANDO_1

	STRUCT PROTA,ACTOR.variables
retardo_frame_paso	byte
frame_paso			byte
switches			byte
	ENDS
