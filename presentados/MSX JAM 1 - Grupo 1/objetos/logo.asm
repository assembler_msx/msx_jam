class_logo:
;Esta es la etiqueta que se debe cargar en HL antes de llamar a inserta_objeto para crear una instancia de esta CLASE


	macro_nombre_objeto 'logo    ' 
	;si depurando_nombres vale 1, esta macro genera un nombre que se podrá ver en la zona de memoria apuntada por IX durante la ejecución del MOV del objeto
	;si depurando_nombres vale 0, la misma macro rellena ese valor con 8 espacios en blanco
	;el nombre de objeto debe tener exactamente 8 caracteres. Si el nombre es más corto, rellenar con espacios
	dw class_logo_init 
		;etiqueta de la función que se ejecutará al insertar el objeto.
		;se utilizará para inicializar las variables propias de ese objeto
		;al llamar a inserta_objeto, se le podrán pasar valores a través de los registros:
		;A,B,C,D,E
		
	dw class_logo_mov
		;etiqueta de la función que se ejecutará en cada frame
		
	dw class_logo_hit
		;etiqueta de la función que se ejecutará cuando este objeto colisione con otro
		;las propiedades width,height,left y top definen la ventana de colisión de cada objeto
		;la ventana de colisión de un objeto puede ser más grande o pequeña que el gráfico de ese objeto
		;el motor revisa las colisiones entre todos los objetos que tengan el bit 


class_logo_init:

	ld [ix+LOGO.tiempo],200
	
	;Para cada sprite del objeto, el motor calcula la suma de ACTOR.y, ACTOR.x, ACTOR.pat y ACTOR.col
	;con ACTOR.spriteN_y, ACTOR.spriteN_x, ACTOR.spriteN_pat y ACTOR.spriteN_col
	;dependiendo de las necesidades, bastará con modificar las propiedades del ACTOR, que afectarán a todos los sprites
	;o spriteN_propiedad, que solo afectarán a ese sprite

	
	ret
	
class_logo_mov:
	dec [ix+LOGO.tiempo]
	ret nz
	
	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
	call siguiente_estado
	
	ret

class_logo_hit:
	ret
	
	STRUCT LOGO,ACTOR.variables
tiempo			byte
	ENDS
		