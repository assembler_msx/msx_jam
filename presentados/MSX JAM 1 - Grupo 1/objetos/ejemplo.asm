class_ejemplo:
;Esta es la etiqueta que se debe cargar en HL antes de llamar a inserta_objeto para crear una instancia de esta CLASE


	macro_nombre_objeto 'ejemplo ' 
	;si depurando_nombres vale 1, esta macro genera un nombre que se podrá ver en la zona de memoria apuntada por IX durante la ejecución del MOV del objeto
	;si depurando_nombres vale 0, la misma macro rellena ese valor con 8 espacios en blanco
	;el nombre de objeto debe tener exactamente 8 caracteres. Si el nombre es más corto, rellenar con espacios
	dw class_ejemplo_init 
		;etiqueta de la función que se ejecutará al insertar el objeto.
		;se utilizará para inicializar las variables propias de ese objeto
		;al llamar a inserta_objeto, se le podrán pasar valores a través de los registros:
		;A,B,C,D,E
		
	dw class_ejemplo_mov
		;etiqueta de la función que se ejecutará en cada frame
		
	dw class_ejemplo_hit
		;etiqueta de la función que se ejecutará cuando este objeto colisione con otro
		;las propiedades width,height,left y top definen la ventana de colisión de cada objeto
		;la ventana de colisión de un objeto puede ser más grande o pequeña que el gráfico de ese objeto
		;el motor revisa las colisiones entre todos los objetos que tengan el bit 


class_ejemplo_init:

	ld [ix+ACTOR.x],c ;parámetro pasado a través del registro C antes de llamar a inserta_objeto
	ld [ix+ACTOR.y],b ;parámetro pasado a través del registro B antes de llamar a inserta_objeto
	
	ld [ix+ACTOR.num_sprites],1 ;el objeto tendrá 1 sprite
	ld [ix+ACTOR.sprite1_y],0 ;el sprite1 estará desplazado 0 pixeles respecto a ACTOR.y
	ld [ix+ACTOR.sprite1_x],0 ;el sprite1 estará desplazado 0 pixeles respecto a ACTOR.x
	ld [ix+ACTOR.sprite1_pat],11*4 ;el pattern del sprite1 será ACTOR.pat+ACTOR.sprite1_pat
	ld [ix+ACTOR.sprite1_col],12 ;el color del sprite1 será ACTOR.col+ACTOR.sprite1_col
	
	;Para cada sprite del objeto, el motor calcula la suma de ACTOR.y, ACTOR.x, ACTOR.pat y ACTOR.col
	;con ACTOR.spriteN_y, ACTOR.spriteN_x, ACTOR.spriteN_pat y ACTOR.spriteN_col
	;dependiendo de las necesidades, bastará con modificar las propiedades del ACTOR, que afectarán a todos los sprites
	;o spriteN_propiedad, que solo afectarán a ese sprite

	
	ret
	
class_ejemplo_mov:
	inc [ix+ACTOR.x]
	ret

	;para eliminar un objeto, basta con hacer 
	set SWITCHES_ACTOR.ELIMINAR,[ix+ACTOR.switches]
	;aunque este no se ejecutará por estar después del RET del evento MOV del objeto
	
	ret

class_ejemplo_hit:
	ret
	
	STRUCT EJEMPLO,ACTOR.variables
cualquier_variable_propia1	byte
cualquier_variable_propia2	byte
cualquier_variable_propia3	byte
cualquier_variable_propia4	byte
	ENDS
	