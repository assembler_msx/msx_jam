class_bottle:
	macro_nombre_objeto 'bottle  '
	dw class_bottle_init
	dw class_bottle_mov
	dw class_bottle_hit


class_bottle_init:
	set SWITCHES_ACTOR.PASIVO,[ix+ACTOR.switches]

	ld [ix+ACTOR.num_sprites],2
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],12*4
	ld [ix+ACTOR.sprite1_col],7
	ld [ix+ACTOR.sprite2_y],0
	ld [ix+ACTOR.sprite2_x],0
	ld [ix+ACTOR.sprite2_pat],13*4
	ld [ix+ACTOR.sprite2_col],15


	ld [ix+KEY.frame],0
	ld [ix+KEY.retardo_frame_paso],1
	ld [ix+KEY.retardo_paso],1

	;ld [ix+ACTOR.x],64
	;ld [ix+ACTOR.y],64
   	ld [ix+ACTOR.x], c
	ld [ix+ACTOR.y], b

	ld [ix+ACTOR.pat],0*4

	ld [ix+ACTOR.left], 2
	ld [ix+ACTOR.top], 2
	ld [ix+ACTOR.width], 12
	ld [ix+ACTOR.height], 12

	ld [ix+ACTOR.pvida],0           ;El objeto key no esta vivo (pvida=0)

	ret
	
class_bottle_mov:
    ;
	;dec [ix+KEY.retardo_frame_paso]
	;ret nz
	
	;ld a,[ix+KEY.frame]
	;inc a
	;and 00000011b
	;ld [ix+KEY.frame],a


	;ld hl,key_frames
	;ADD_HL_A
	;ld a,[hl]
	;ld [ix+ACTOR.pat],a

	;ld hl,key_retardo_frames
	;ld a,[ix+KEY.frame]
	;ADD_HL_A
	;ld a,[hl]
	;ld [ix+KEY.retardo_frame_paso],a

	ret


class_bottle_hit:
    bit SWITCHES_ACTOR.PROTA, [iy+ACTOR.switches]
    ret Z

    set SWITCHES_ACTOR.ELIMINAR, [ix+ACTOR.switches]
    
    ld hl,class_bottle  ;insertamos la botella en el inventario
	ld c,232
	ld b,16
	call inserta_objeto
    
    ret
	
;bottle_frames: db 0*4,1*4,2*4,1*4
;bottle_retardo_frames: db 9,3,5,3


	STRUCT BOTTLE,ACTOR.variables
retardo_frame_paso	byte
retardo_paso		byte
frame			    byte
	ENDS
	