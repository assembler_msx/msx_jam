carga_graficos_logo:

	;limpiamos la pantalla
	ld hl,6144
	ld bc,32*24
	xor a
	call filvrm

	ld a,15
	ld [0xf3e9],a
	ld a,4
	ld [0xf3ea],a
	ld [0xf3eb],a
	
	call 0x0062 ;CHGCLR cambia el color de letras en función a los valores de FORCLR, BAKCLR y BDRCLR


	;cargamos los mismos tiles en los tres bloques
	ld a,4
	selecciona_segmento_8_megarom

	ld hl,logo_grupo_tiles_forma
	ld de,256*8*0
	ld bc,fin_logo_grupo_tiles_forma-logo_grupo_tiles_forma
	call ldirvm

	ld hl,logo_grupo_tiles_forma
	ld de,256*8*1
	ld bc,fin_logo_grupo_tiles_forma-logo_grupo_tiles_forma
	call ldirvm

	ld hl,logo_grupo_tiles_forma
	ld de,256*8*2
	ld bc,fin_logo_grupo_tiles_forma-logo_grupo_tiles_forma
	call ldirvm




	ld hl,logo_grupo_tiles_color
	ld de,256*8*0+8192
	ld bc,fin_logo_grupo_tiles_color-logo_grupo_tiles_color
	call ldirvm

	ld hl,logo_grupo_tiles_color
	ld de,256*8*1+8192
	ld bc,fin_logo_grupo_tiles_color-logo_grupo_tiles_color
	call ldirvm

	ld hl,logo_grupo_tiles_color
	ld de,256*8*2+8192
	ld bc,fin_logo_grupo_tiles_color-logo_grupo_tiles_color
	call ldirvm

	ld hl,6144+256+32*3
	ld b,64

.otrotile:
	ld a,64
	sub b
	call wrtvrm
	inc hl
	djnz .otrotile
	
	ret
	