aux: 				# 2
aux2: 				# 2
aux3: 				# 2
nodo_padre: 			# 2 ;el inicio de cada ciclo al recorrer los objetos, se almacena el objeto actual en esta variable. Así si un objeto necesita saber quien lo ha creado, solo tiene que consultar esta variable
					;solo se van a crear objetos en el recorrido MOV o HIT
                 ;solo se van a crear objetos en el recorrido MOV o HIT
nodo_anterior: 			# 2 ;para el proceso de eliminación,
nodo_anterior_aux: 		# 2 ;para el proceso de eliminación,
nodos_ocupados: 		# 2
nodos_libres: 			# 2
ultimo_nodo_ocupado: 		# 2


;Punteros para la comprobación de colisión entre objetos
;En cada ciclo recorre_nodos_mov_paint_eliminar, dependiendo del valor de switches,
;se coloca cada objeto en una de las siguientes listas
;las listas se limpian en cada ciclo, para no tener que estar pendiente de eliminaciones, insercciones
;los tipos de objetos para colisión son :
;pasivos: los objetos que al recibir un impacto de bala de paulo, deben hacer algo (morir, dar un objeto, etc)
;activos: los objetos que pueden impactar contra los pasivos (balas, paulo)
;los pasivos no se deben comprobar entre sí, ni los activos entre sí.
;Cuando se ha hecho todo el recorrido, comprobamos cada activo con cada pasivo. Si hay colisión, ejecutamos el hit de los dos objetos.

objetos_activos: 		# 2*NODOS_MAX
objetos_pasivos: 		# 2*NODOS_MAX

indice_objetos_activos: 	# 2
indice_objetos_pasivos: 	# 2
indice_objetos_pasivos_bucle: 	# 2
num_objetos_pasivos_bucle: 	# 1
estado_partida: 		# 1 ;se utiliza para salir del bucle principal de control de objetos:
; 0: ciclo normal
; 1: salida por perdida de vida
; 2: salida por game over
; 3: salida por fin de fase
; 4: inicio partida desde menu


ultimo_objeto_creado:		#2
num_objetos: 			#1



cuantos_megasprites_de_4 equ 5 ;empieza la SPAT en 0x7600
cuantos_megasprites_de_2 equ 4 ;empieza la SPAT en 0x7600+4*4*5=0x7650
cuantos_megasprites_de_1 equ 4 ;empieza la SPAT en 0x7600+4*4*5+2*4*4=0x7670



slots_sprites_libres_x4:	#1 ;indica, bit a bit, que bloque de sprites, de 4 unidades, está libre. BITs 0 a 4: 5 megasprite de 4=20
slots_sprites_libres_x2:	#1 ;indica, bit a bit, que bloque de sprites, de 2 unidades, está libre. BITs 0 a 3: 4 megasprite de 2=8
slots_sprites_libres_x1:	#1 ;indica, bit a bit, que bloque de sprites, de 1 unidades, está libre. BITs 0 a 3: 4 megasprite de 1=4

sprites_frame_anterior: 	#1
sprites_frame_actual:		#1


parametros_desde_mapa_a: 	#1
parametros_desde_mapa_e: 	#1
parametros_desde_mapa_d: 	#1
parametros_desde_mapa_c: 	#1
parametros_desde_mapa_b: 	#1

;estas variables tienen que estar en este orden y seguidas.
;en la inicialización de recorre_nodos_mov_paint_y_eliminar se inicializan a cero con un ldir
num_objetos_activos: 		# 1
num_objetos_pasivos: 		# 1
relleno:			# 1 ;para poder poner a 0 estas variables con un ld [sp0],hl ,ld [sp0+2],hl .... necesitamos que las variables sean palabras

num_sprites_sw_restaurar:	#1
num_sprites_sw_pintar:		#1
puntero_lista_sprites_sw_restaurar:	#2
puntero_lista_sprites_sw_pintar:		#2

lista_sprites_sw_restaurar:	#2*MAX_SPRITES_SW
lista_sprites_sw_pintar:		#2*MAX_SPRITES_SW

;HASTA AQUI

inicio_nodos: 			# 0
	if (inicio_nodos>0xd000) 
		error "Chacho, que machacas los nodos"
	endif

	map 0xd000
nodos: #  NODOS_MAX*NODOS_SIZE ;64 nodos de 64 bytes, total 4kb
