class_prota:
	macro_nombre_objeto 'prota   '
	dw class_prota_init
	dw class_prota_mov
	dw class_prota_hit


class_prota_init:
	ld [ix+ACTOR.num_sprites],4
	
	set SWITCHES_ACTOR.ACTIVO,[ix+ACTOR.switches]
	
	ld [ix+ACTOR.sprite1_y],0
	ld [ix+ACTOR.sprite1_x],0
	ld [ix+ACTOR.sprite1_pat],0*4
	ld [ix+ACTOR.sprite1_col],15
	ld [ix+ACTOR.sprite2_y],0
	ld [ix+ACTOR.sprite2_x],0
	ld [ix+ACTOR.sprite2_pat],1*4
	ld [ix+ACTOR.sprite2_col],9
	ld [ix+ACTOR.sprite3_y],0
	ld [ix+ACTOR.sprite3_x],0
	ld [ix+ACTOR.sprite3_pat],2*4
	ld [ix+ACTOR.sprite3_col],6
	
	
	

	ld [ix+ACTOR.x],100
	ld [ix+ACTOR.y],104
	ld [ix+ACTOR.pat],SPRITE_PROTA_PARADO


	ld [ix+ACTOR.left],4
	ld [ix+ACTOR.top],2
	ld [ix+ACTOR.width],8
	ld [ix+ACTOR.height],12
	
	
	ld [ix+PROTA.retardo_frame_paso],1
	ld [ix+PROTA.frame_paso],0
	ld [ix+PROTA.switches],0

	ld a,[pvida_prota]
	ld [ix+ACTOR.pvida],a
	ret
	
class_prota_mov:

	ld [ix+ACTOR.num_sprites],3
	

	call prota_gestion_andar

	call prota_gestion_pasos

	ret

class_prota_hit:
	ld a,[iy+ACTOR.pvida] ;si el objeto con el que ha colisionado el prota tiene pvida, recibe un impacto
	or a
	ret z
	set SWITCHES_ACTOR.ELIMINAR,[iy+ACTOR.switches]
	dec [ix+ACTOR.pvida]
	jr nz,.vida_perdida
	call reset_estado
	ret
.vida_perdida:
	ld a,[ix+ACTOR.pvida]
	ld [pvida_prota],a
	call mismo_estado
	ret
	

prota_gestion_andar:
;1000  Der Aba Arr Izq Del Ins Home Space
	res SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	
	ld a,[buffer_teclado8]
	bit 7,a
	call z,.derecha
	ld a,[buffer_teclado8]
	bit 4,a
	call z,.izquierda
	ld a,[buffer_teclado8]
	bit 5,a
	call z,.arriba
	ld a,[buffer_teclado8]
	bit 6,a
	call z,.abajo

	call comprueba_salida
	
	ret
.derecha:
	ld b,15
	ld c,11
	call comprueba_tile_duro
	ret z
	inc [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret nz
	ld [ix+PROTA.retardo_frame_paso],1
	set SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.izquierda:
	ld b,15
	ld c,5
	call comprueba_tile_duro
	ret z

	dec [ix+ACTOR.x]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret z
	ld [ix+PROTA.retardo_frame_paso],1
	res SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	ret
.arriba:
	ld b,14
	ld c,6
	call comprueba_tile_duro
	ret z
	ld b,14
	ld c,10
	call comprueba_tile_duro
	ret z

	dec [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
.abajo:
	ld b,16
	ld c,6
	call comprueba_tile_duro
	ret z
	ld b,16
	ld c,10
	call comprueba_tile_duro
	ret z
	inc [ix+ACTOR.y]
	set SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret
	
		
prota_gestion_pasos:
	bit SWITCHES_PROTA.ANDANDO,[ix+PROTA.switches]
	ret z
	dec [ix+PROTA.retardo_frame_paso]
	ret nz
	ld [ix+PROTA.retardo_frame_paso],8
	
	ld a,[ix+PROTA.frame_paso]
	xor 00000001b 
	ld [ix+PROTA.frame_paso],a
	ld hl,frames_prota
	ADD_HL_A
	ld a,[hl]
	bit SWITCHES_PROTA.DERECHA,[ix+PROTA.switches]
	jr nz,.mirando_derecha
	add 6*4
.mirando_derecha:	
	ld [ix+ACTOR.pat],a
	
	ret
	
comprueba_salida:
	ld a,[ix+ACTOR.x]
	cp 2
	jr c,.salida_por_izquierda
	cp 256-16
	jr nc,.salida_por_derecha
	ld a,[ix+ACTOR.y]
	cp 1
	jr c,.salida_por_arriba
	cp 192-16
	jr nc,.salida_por_abajo
	ret
.salida_por_izquierda:
	ld a,256-16
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	
	ld a,1
	call ayFX_INIT
	ret
.salida_por_derecha:
	ld a,3
	ld [ix+ACTOR.x],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret

.salida_por_arriba:
	ld a,192-16
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	xor a
	sbc hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret
.salida_por_abajo:
	ld a,3
	ld [ix+ACTOR.y],a
	
	ld hl,[puntero_mapa_stage]
	ld de,ancho_total_pantallas*alto_a_dibujar
	add hl,de
	ld [puntero_mapa_stage],hl
	call dibuja_mapa
	ld a,1
	call ayFX_INIT
	ret
		
		
		
		
		
		
		
		
		
		
SPRITE_PROTA_PARADO equ 0*4

SPRITE_PROTA_ANDANDO_0 equ 0*4
SPRITE_PROTA_ANDANDO_1 equ 3*4

	STRUCT SWITCHES_PROTA
	;se utilizan tipos de dato byte, pero se utilizan como posiciones de bit dentro de PROTA.switches
ANDANDO			byte
LIBRE1			byte
LIBRE2			byte
LIBRE3			byte
DERECHA			byte
LIBRE5			byte
LIBRE6			byte
LIBRE7			byte
	ENDS


frames_prota:
	db SPRITE_PROTA_ANDANDO_0
	db SPRITE_PROTA_ANDANDO_1

	STRUCT PROTA,ACTOR.variables
retardo_frame_paso		byte
frame_paso			byte
switches			byte
	ENDS
